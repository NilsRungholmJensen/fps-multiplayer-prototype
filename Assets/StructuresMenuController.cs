﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StructuresMenuController : MonoBehaviour, IBuilderPlayerDependant
{
    [SerializeField] internal Transform buildingButtonContainer;
    [SerializeField] internal GameObject buildButtonPrefab;
    [SerializeField] internal Button removeStructureButton;

    private BuilderPlayerController playerController;
    private PlaceStructureFeature placeStructureFeature;
    private RemoveStructureFeature removeStructureFeature;
    

    public Action<BuilderPlayerController> OnStartPlayerAuthority => OnStartAuthority;

    private void OnStartAuthority(BuilderPlayerController playerController)
    {
        this.playerController = playerController;
        this.placeStructureFeature = playerController.GetComponent<PlaceStructureFeature>();
        this.removeStructureFeature = playerController.GetComponent<RemoveStructureFeature>();

        if (!placeStructureFeature)
            return;

        PopulateStructuresMenu(playerController.CurrentStructureLibrary.AvalibleStructures);
    }

    private void PopulateStructuresMenu(List<StructureDefinition> avalibleStructures)
    {
        foreach(StructureDefinition structure in avalibleStructures)
        {
            Button newButton = Instantiate(buildButtonPrefab, buildingButtonContainer).GetComponentInChildren<Button>();
            newButton.onClick.AddListener(() => { TogglePlaceStructureFeatures(true); placeStructureFeature.SetSelectedStructure(structure.uniqueName); });
            newButton.GetComponentInChildren<Text>().text = structure.uniqueName;
        }
    }

    private void TogglePlaceStructureFeatures(bool placing)
    {
        placeStructureFeature.enabled = placing; removeStructureFeature.enabled = !placing;
    }

    public Action<BuilderPlayerController> OnStopPlayerAuthority => OnStopAuthority;

    private void OnStopAuthority(BuilderPlayerController playerController)
    {
        if (!placeStructureFeature)
            return;

        removeStructureButton.onClick.RemoveAllListeners();
    }
}
