﻿using JetBrains.Annotations;
using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlaceStructureFeature : BuilderPlayerFeature
{
    [SerializeField] private Material ghostMaterialAllowed;
    [SerializeField] private Material ghostMaterialDisallowed;
    [SerializeField] private StructureDefinition buildersConsoleDefinition;
    [SerializeField] private StructureDefinition factoryDefinition;
    [SerializeField] private StructureDefinition barraicadeDefinition;
    [SerializeField] private StructureDefinition wallDefinition;
    [SerializeField] private StructureDefinition wallCornerDefinition;
    [SerializeField] private StructureDefinition pipeDefinition;

    private GameObject ghostObject;
    private IEnumerable<MeshRenderer> allGhostObjectRenderers;

    internal StructureDefinition SelectedStructure
    {
        get { return selectedStructure; }
        set {
            selectedStructure = value;
            CreateAndAssignStructureGhost(ref ghostObject, selectedStructure);
        }
    }
    private StructureDefinition selectedStructure;

    private void CreateAndAssignStructureGhost(ref GameObject targetGhostObject, StructureDefinition structureBase)
    {
        if (targetGhostObject != null)
        {
            Destroy(targetGhostObject);
        }

        Type[] allowedTypes = { typeof(Transform), typeof(MeshFilter), typeof(MeshRenderer) };

        GameObject newGhost = Instantiate(structureBase.structurePrefab);
        IEnumerable<Component> components = newGhost.GetComponentsInChildren<Component>();
        IEnumerable<Component> redundantComponents = components.Where(x => !allowedTypes.Contains(x.GetType()));

        //TODO: Change this to a more clever approach, where we don't have to hack the fact that components depend on each other
        foreach (Component c in redundantComponents)
        {
            Destroy(c);
        }
        foreach (Component c in redundantComponents)
        {
            Destroy(c);
        }

        allGhostObjectRenderers = components.Where(x => x.GetType() == typeof(MeshRenderer)).Select(x => (MeshRenderer)x);

        foreach (MeshRenderer m in allGhostObjectRenderers)
        {
            m.material = ghostMaterialAllowed;
        }

        targetGhostObject = newGhost;
    }

    
    private Vector3 cachedGhostPosition;

    private bool vacancyAtGhostPosition;
    private ulong nextStructureGuid;

    public void SetSelectedStructure(string structureName)
    {
        switch (structureName)
        {
            case "buildersConsole":
                {
                    SelectedStructure = buildersConsoleDefinition;
                } break;
            case "factory":
                {
                    SelectedStructure = factoryDefinition;
                } break;
            case "barricade":
                {
                    SelectedStructure = barraicadeDefinition;
                } break;
            case "wall":
                {
                    SelectedStructure = wallDefinition;
                } break;
            case "wallCorner":
                {
                    SelectedStructure = wallCornerDefinition;
                } break;
            case "pipe":
                {
                    SelectedStructure = pipeDefinition;
                } break;
            default:
                {
                    SelectedStructure = null;
                } break;
        }
    }

    private void Update()
    {
        bool selectedStructureIsNull = (SelectedStructure == null);

        if (selectedStructureIsNull || isPointerOverUi() || (ghostObject == null))
            return;

        ghostObject.SetActive(!selectedStructureIsNull && !isPointerOverUi());

        //Move ghost
        RaycastHit hit;
        if (Physics.Raycast(PlayerController.playerCamera.ScreenPointToRay(InputHandler.GetCursorPosition(), Camera.MonoOrStereoscopicEye.Mono), out hit, 1000f, ~0))
        {
            Vector3 newGhostPosition = hit.point.ToGridPositionAsVector();

            if (newGhostPosition != cachedGhostPosition)
            {
                ghostObject.transform.position = newGhostPosition;
                vacancyAtGhostPosition = GetIsCellVacant(newGhostPosition.ToGridPosition());

                foreach(MeshRenderer m in allGhostObjectRenderers)
                {
                    m.material = vacancyAtGhostPosition ? ghostMaterialAllowed : ghostMaterialDisallowed;
                }
            }

            cachedGhostPosition = newGhostPosition;
        }

        //Rotate ghost
        if (InputHandler.GetRotateStructureInput_Down())
        {
            ghostObject.transform.Rotate(new Vector3(0f, 90f, 0f));
        }

        //Place structure
        if (vacancyAtGhostPosition)
        {
            if (InputHandler.GetSelectInput_Down())
            {
                if (SelectedStructure != null)
                {
                    TryPlaceStructure(SelectedStructure, ghostObject.transform.position.ToGridPosition(), ghostObject.transform.rotation.ToGridRotation());
                }
            }
        }
    }

    [Client]
    private void TryPlaceStructure(StructureDefinition structure, (int, int) centerGridPosition, GridRotation gridRotation)
    {
        if (structure.structurePrefab != null)
        {
            CmdTryPlaceStructure(structure.uniqueName, centerGridPosition.Item1, centerGridPosition.Item2, gridRotation);
        }
    }

    [Command]
    private void CmdTryPlaceStructure(string structureUniqueName, int centerGridPositionX, int centerGridPositionZ, GridRotation gridRotation)
    {
        StructureDefinition structureDefinition = PlayerController.CurrentStructureLibrary.AvalibleStructures.Where(x => x.uniqueName == structureUniqueName).First();
        if (!structureDefinition)
            throw new InvalidOperationException(structureUniqueName + "wasn't found in BuilderPlayerController's StructureLibrary.");

        //Validate position against actors/dynamic objects AND validate position against structure grid
        bool isPositionFree = GetIsCellVacant((centerGridPositionX, centerGridPositionZ)) && StructureGrid.IsPositionFree((centerGridPositionX, centerGridPositionZ));

        if (!isPositionFree)
        {
            Debug.LogWarning("Tried to place structure at " + centerGridPositionX + "," + centerGridPositionZ + " but the position was occupied");
            return;
        }

        //Create prefab
        GameObject newStructureGO = Instantiate(structureDefinition.structurePrefab, (centerGridPositionX, centerGridPositionZ).ToWorldPosition(), gridRotation.ToQuaternion(), null);
        StructureInstance newStructureInstance= newStructureGO.GetComponent<StructureInstance>();
        (int, int)[] occupyingCells = newStructureInstance.GridBlockers;
        Socket[] sockets = newStructureInstance.Sockets;

        //Register at cell
        if (StructureGrid.TryRegisterStructureAtCells(newStructureInstance, occupyingCells, sockets))
        {
            //Spawn
            NetworkServer.Spawn(newStructureGO);
            GlobalServerController.RebakeLevelNavmesh();
        }
        else
        { Destroy(newStructureGO); }
    }

    private void OnDisable()
    {
        ghostObject?.SetActive(false);
    }
}
