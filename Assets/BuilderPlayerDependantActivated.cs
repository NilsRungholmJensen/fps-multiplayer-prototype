﻿using System;
using UnityEngine;

/// <summary>
/// Placed on objects to automatically activate/deactivate them along with the BuilderPlayer object (based on whether it has authority)
/// </summary>
public class BuilderPlayerDependantActivated : MonoBehaviour, IBuilderPlayerDependant
{
    [SerializeField] private bool startOn = false;

    public Action<BuilderPlayerController> OnStartPlayerAuthority => StartAuthority;
    public Action<BuilderPlayerController> OnStopPlayerAuthority => StopAuthority;

    private void Awake()
    {
        gameObject.SetActive(startOn);
    }

    private void StartAuthority(BuilderPlayerController playerController)
    {
        gameObject.SetActive(true);
    }

    private void StopAuthority(BuilderPlayerController playerController)
    {
        gameObject.SetActive(false);
    }
}