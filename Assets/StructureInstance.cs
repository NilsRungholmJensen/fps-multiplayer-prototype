﻿using Mirror;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

internal class StructureInstance : NetworkBehaviour
{
    [SerializeField] internal StructureDefinition structureDefinition;

    internal Socket[] Sockets => sockets ?? (sockets = GetSockets());
    internal Socket[] sockets;
    internal (int, int)[] GridBlockers => gridBlockers ?? (gridBlockers = GetGridBlockers());
    internal (int, int)[] gridBlockers;

    public override void OnStartServer()
    {
        base.OnStartServer();

        foreach (Socket socket in Sockets)
        {
            
        }
    }

    private Socket[] GetSockets()
    {
        return GetComponentsInChildren<Socket>().ToArray();
    }

    private (int, int)[] GetGridBlockers()
    {
        return GetComponentsInChildren<GridBlocker>().Select(x => x.transform.position.ToGridPosition()).ToArray();
    }
}