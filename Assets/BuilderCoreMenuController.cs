﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BuilderCoreMenuController : MonoBehaviour, IBuilderPlayerDependant
{
    [SerializeField] private Button exitConstructionModeButton;

    BuilderPlayerController playerController;

    public Action<BuilderPlayerController> OnStartPlayerAuthority => StartAuthority;
    public Action<BuilderPlayerController> OnStopPlayerAuthority => StopAuthority;

    private void StartAuthority(BuilderPlayerController playerController)
    {
        this.enabled = true;
        this.playerController = playerController;
        exitConstructionModeButton.onClick.AddListener(EndBuilderSession);
    }

    private void StopAuthority(BuilderPlayerController obj)
    {
        exitConstructionModeButton.onClick.RemoveAllListeners();
        this.enabled = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            EndBuilderSession();
        }
    }

    private void EndBuilderSession()
    {
        this.playerController.TryTransitionToFpsPlayer();
    }
}
