﻿using Mirror;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StructureGrid : NetworkBehaviour
{
    private static StructureGrid Instance => instance != null ? instance : (instance = FindObjectOfType<StructureGrid>());
    private static StructureGrid instance;
    
    private Dictionary<(int, int), StructureInstance> structureGridContents = new Dictionary<(int, int), StructureInstance>();
    private Dictionary<(int, int), Socket[]> socketGridContents = new Dictionary<(int, int), Socket[]>();

    public static float CellSize => StructureGridModel.cellSize;

    #region Get grid information

    internal static StructureInstance GetStructureAtPosition(Vector3 worldPosition)
    {
        return GetStructureAtPosition(worldPosition.ToGridPosition());
    }

    private static StructureInstance GetStructureAtPosition((int, int) gridPosition)
    {
        StructureInstance occupyingStructure;
        if (TryGetGridStructure(gridPosition, out occupyingStructure))
        {
            return occupyingStructure;
        }

        return null;
    }

    internal static bool IsPositionFree(Vector3 worldPosition)
    {
        return IsPositionFree(worldPosition.ToGridPosition());
    }

    internal static bool IsPositionFree((int, int) gridPosition)
    {
        return !Instance.structureGridContents.ContainsKey(gridPosition);
    }

    private static bool TryGetGridStructure((int, int) gridPosition, out StructureInstance occupyingStructure)
    {
        return Instance.structureGridContents.TryGetValue(gridPosition, out occupyingStructure);
    }

    private static bool TryGetGridSockets((int, int) gridPosition, out Socket[] occupyingSockets)
    {
        return Instance.socketGridContents.TryGetValue(gridPosition, out occupyingSockets);
    }

    #endregion

    [Server]
    internal static bool TryRegisterStructureAtCell((int, int) gridPosition, StructureInstance structureInstance)
    {
        StructureInstance occupyingStructure;
        if (TryGetGridStructure(gridPosition, out occupyingStructure))
        {
            return false;
        }

        Instance.structureGridContents.Add(gridPosition, structureInstance);
        return true;
    }

    [Server]
    internal static bool TryRegisterStructureAtCells(StructureInstance newStructureInstance, (int, int)[] occupyingCells)
    {
        return TryRegisterStructureAtCells(newStructureInstance, occupyingCells, new Socket[0]);
    }

    [Server]
    internal static bool TryRegisterStructureAtCells(StructureInstance newStructureInstance, (int, int)[] newOccupyingCells, Socket[] newSockets)
    {
        StructureInstance[] cell = new StructureInstance[newOccupyingCells.Length];

        //Make sure all positions are free
        for (int i = 0; i < newOccupyingCells.Length; i++)
        {
            if (TryGetGridStructure(newOccupyingCells[i], out cell[i]))
            {
                //TODO: Consider making it so that you can replace structures with the same definition
                return false;
            }
        }

        //Only if ALL positions are free, register each of them
        for (int i = 0; i < newOccupyingCells.Length; i++)
        {
            Instance.structureGridContents.Add(newOccupyingCells[i], newStructureInstance);
        }

        //..then register all the structure's sockets
        for (int i = 0; i < newSockets.Length; i++)
        {
            Socket currentSocket = newSockets[i];

            // (1) Register outer sockets (using outer-cell positions)

            (int, int) outerPos = newSockets[i].OuterCell;
            Socket[] occupyingSockets;

            //If already existing sockets:
            if (TryGetGridSockets(outerPos, out occupyingSockets))
            {
                //Replace entry with concatenated array of new + old sockets
                Instance.socketGridContents[outerPos] = occupyingSockets.Concat(newSockets).ToArray();
            }
            //If no existing sockets:
            else
            {
                Instance.socketGridContents.Add(outerPos, new Socket[1] { currentSocket });
            }

            // (2) Check if any of the inner sockets have hit a position that allows them to connect

            (int, int) innerPos = newSockets[i].InnerCell;
            Socket[] matchingSockets;

            if (TryGetGridSockets(innerPos, out matchingSockets))
            {
                foreach(Socket match in matchingSockets)
                {
                    if (match.OutwardsDirection == currentSocket.InwardsDirection && match.TryConnect(newSockets[i]))
                        break;
                }
            }
        }

        return true;
    }

    

    private void OnDrawGizmos()
    {
        const int gizmoGridCellCount = 128;
        int fromX = StructureGridModel.gridHalfSizeX - gizmoGridCellCount / 2;
        int fromZ = StructureGridModel.gridHalfSizeZ - gizmoGridCellCount / 2;
        int toX = StructureGridModel.gridHalfSizeX + gizmoGridCellCount / 2;
        int toZ = StructureGridModel.gridHalfSizeZ + gizmoGridCellCount / 2;

        for (int x = fromX; x < toX; x++)
        {
            for (int z = fromZ; z < toZ; z++)
            {
                StructureInstance occupyingStructure;
                Socket[] occupyingSockets;
                bool hasStructure = TryGetGridStructure((x, z), out occupyingStructure);
                bool hasSockets = TryGetGridSockets((x, z), out occupyingSockets);

                Gizmos.color = hasStructure ? Color.red : Color.green;
                Gizmos.DrawWireCube((x, z).ToWorldPosition(), new Vector3(1f, 0.02f, 1f) * StructureGridModel.cellSize * 0.995f);

                if (hasSockets)
                {
                    foreach (Socket socket in occupyingSockets)
                    {
                        DrawSocketGizmo(socket);
                    }
                }
            }
        }

        void DrawSocketGizmo(Socket socket)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(Vector3.Lerp(socket.InnerCell.ToWorldPosition(), socket.OuterCell.ToWorldPosition(), 0.45f), Vector3.one * StructureGridModel.cellSize * 0.02f);

            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(Vector3.Lerp(socket.InnerCell.ToWorldPosition(), socket.OuterCell.ToWorldPosition(), 0.55f), Vector3.one * StructureGridModel.cellSize * 0.02f);

            Gizmos.color = socket.IsConnected ? Color.cyan : Color.blue;
            Gizmos.DrawWireCube(Vector3.Lerp(socket.InnerCell.ToWorldPosition(), socket.OuterCell.ToWorldPosition(), 0.5f), Vector3.one * StructureGridModel.cellSize * 0.15f);
        }
    }

    
}

public enum GridRotation
{
    N = 0,
    W = 1,
    S = 2,
    E = 3
}