﻿using System;

internal interface IPlayerDependant
{
}

/// <summary>
/// Indicates that a component is dependant on the GenericPlayer object existing and having authority (i.e. the below methods will ONLY be called when from the local player, and encompasses the session-loop)
/// The generic player is the player-controller responsible for features shared across the entire game-loop from joining/hosting to exiting to menu - such as showing the escape-menu
/// </summary>
internal interface IGenericPlayerDependant : IPlayerDependant
{
    Action<GenericPlayerController> OnStartPlayerAuthority { get; }
    Action<GenericPlayerController> OnStopPlayerAuthority { get; }
}

/// <summary>
/// Indicates that a component is dependant on the FpsPlayer object existing AND having authority
/// (i.e. the below methods will ONLY be called when the local player's state changes)
/// </summary>
internal interface IFpsPlayerDependant : IPlayerDependant
{
    Action<FpsPlayerController> OnStartPlayerAuthority { get; }
    Action<FpsPlayerController> OnStopPlayerAuthority { get; }
}

/// <summary>
/// Indicates that a component is dependant on the BuilderPlayer object existing AND having authority
/// (i.e. the below methods will ONLY be called when the local player's state changes)
/// </summary>
internal interface IBuilderPlayerDependant : IPlayerDependant
{
    Action<BuilderPlayerController> OnStartPlayerAuthority { get; }
    Action<BuilderPlayerController> OnStopPlayerAuthority { get; }
}