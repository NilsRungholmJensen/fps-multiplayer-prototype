﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item Definition", menuName = "Builder Assets/Item Definition")]
internal class ItemDefinition : ScriptableObject
{
    [SerializeField] internal GameObject itemPrefab;
}