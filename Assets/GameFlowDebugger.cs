﻿using Mirror;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GameFlowDebugger : NetworkBehaviour
{
    [SerializeField] private GameObject enemyPrefab;
    private List<Vector3> enemySpawnPositions;

    public override void OnStartServer()
    {
        enemySpawnPositions = FindObjectsOfType<EnemySpawnpoint>().Select(x => x.transform.position).ToList();
    }

    [Server]
    public void SpawnEnemies(int count)
    {
        Debug.Log("SpawningEnemy");
        for (int i = 0; i < count; i++)
        {
            int n = Random.Range(0, enemySpawnPositions.Count);

            NavMeshHit hit;
            NavMesh.SamplePosition(enemySpawnPositions[n], out hit, 1.0f, 1);
            GameObject spawnedEnemy = Instantiate(enemyPrefab, hit.position + Vector3.up*0.9f, Quaternion.LookRotation(-enemySpawnPositions[n], Vector3.up), null);

            NetworkServer.Spawn(spawnedEnemy);
        }
    }
}
