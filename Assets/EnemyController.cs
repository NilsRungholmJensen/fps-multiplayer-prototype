﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    private GameObject currentTarget;

    // Start is called before the first frame update
    void Start()
    {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        currentTarget = GameObject.Find("PowerDrill");

        agent.destination = currentTarget?.transform?.position ?? Vector3.zero;
    }
}
