﻿/// <summary>
/// All implementing members set the layer of the 
/// </summary>
internal interface ILayerSetter
{
    int Layer { get; }
}