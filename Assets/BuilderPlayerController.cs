﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BuilderInputProvider))]
public class BuilderPlayerController : AbstractPlayerController
{
    protected IBuilderInputHandler InputHandler => (inputHandler != null) ? inputHandler : (inputHandler = GetComponent<BuilderInputProvider>().Handler);
    protected IBuilderInputHandler inputHandler;

    [Tooltip("DEBUG: Make the entire structure-library avaliable to the player")] [SerializeField] internal bool alwaysUseMasterLibrary;

    [SerializeField] private StructureLibrary masterStructureLibrary;
    internal StructureLibrary CurrentStructureLibrary => alwaysUseMasterLibrary ? masterStructureLibrary : currentStructureLibrary;
    [SerializeField] private StructureLibrary currentStructureLibrary;
    private const float fieldOfView = 35f;

    public override void OnStartAuthority()
    {
        if (!hasAuthority)
            return;

        Debug.Log("Builder for player with netId "+netId+": START authority!");
        //Place main camera in this transform
        playerCamera = Camera.main;
        playerCamera.transform.SetParent(transform.Find("CameraTarget").transform, false);

        playerCamera.transform.localPosition = new Vector3(0, 0, 0);
        playerCamera.transform.localRotation = Quaternion.identity;
        playerCamera.fieldOfView = fieldOfView;

        //Unlock cursor
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        foreach (IBuilderPlayerDependant playerDependant in UnityEngineExtensions.FindAllComponentsInScene().OfType<IBuilderPlayerDependant>())
        {
            playerDependant.OnStartPlayerAuthority?.Invoke(this);
        }
    }

    public override void OnStopAuthority()
    {
        if (hasAuthority)
            return;

        //If player camera is still present as child..
        if (playerCamera != null && playerCamera.transform.parent == transform.Find("CameraTarget").transform)
        {
            playerCamera.transform.SetParent(null, false);

            //Return main camera to root transform

            playerCamera.transform.localPosition = new Vector3(0, 0, 0);
            playerCamera.transform.localRotation = Quaternion.identity;

            playerCamera = null;
        }

        foreach (IBuilderPlayerDependant playerDependant in UnityEngineExtensions.FindAllComponentsInScene().OfType<IBuilderPlayerDependant>())
        {
            playerDependant.OnStopPlayerAuthority?.Invoke(this);
        }

        //Debug.Log("Builder for player with netId " + netId + ": STOP authority!");
    }

    [Client]
    internal void TryTransitionToFpsPlayer()
    {
        CmdTryTransitionToFpsPlayer();
    }

    [Command]
    internal void CmdTryTransitionToFpsPlayer()
    {
        GlobalServerController.TransitionToFpsPlayer(this.netIdentity);
    }
}
