﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructable : NetworkBehaviour
{
    [SyncVar] [SerializeField] private float hitPoints = 100;
    [SyncVar] private bool destroyed = false;

    private void OnEnable()
    {
        //Subscribe to damage
        foreach (Damagable damagable in GetComponentsInChildren<Damagable>())
        {
            damagable.OnDamaged += this.OnHit;
        }
    }

    private void OnDisable()
    {
        //Unsubscribe from damage
        foreach (Damagable damagable in GetComponentsInChildren<Damagable>())
        {
            damagable.OnDamaged -= this.OnHit;
        }
    }

    private void OnHit(float damage)
    {
        //Inform the server
        CmdOnHit(damage);
    }

    //Server only

    [Command(requiresAuthority = false)]
    private void CmdOnHit(float damage, NetworkConnectionToClient sender = null)
    {
        hitPoints -= damage;

        if (hitPoints <= 0)
        {
            Debug.Log("Server confirms the destruction!");

            destroyed = true;

            //Unsubscribe from damage
            foreach (Damagable damagable in GetComponentsInChildren<Damagable>())
            {
                damagable.OnDamaged -= this.OnHit;
            }

            //Inform all clients
            RpcOnDestruction();

            StartCoroutine(DestorySelfAfterDelay(0.5f));
        }
    }

    private IEnumerator DestorySelfAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        NetworkServer.Destroy(gameObject);
        if (gameObject.layer == LayerMask.NameToLayer("NavMeshModifier"))
        {
            GlobalServerController.RebakeLevelNavmesh();
        }
    }

    //Clients only

    [ClientRpc]
    private void RpcOnDestruction()
    {
        //GetComponent<Animator>()?.Play("Die");
    }
}
