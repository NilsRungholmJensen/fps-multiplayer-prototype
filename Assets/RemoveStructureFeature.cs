﻿using Mirror;
using System.Linq;
using UnityEngine;

public class RemoveStructureFeature : BuilderPlayerFeature
{
    [SerializeField] private GameObject ghostPrefab;

    private GameObject GhostObject => ghostObject ? ghostObject : (ghostObject = Instantiate(ghostPrefab, gameObject.transform));
    private GameObject ghostObject;

    private StructureInstance hoveredStructure;
    private Vector3 cachedGhostPosition;

    private void Update()
    {
        GhostObject.SetActive(!isPointerOverUi());

        if (isPointerOverUi())
            return;

        RaycastHit hit;

        if (Physics.Raycast(PlayerController.playerCamera.ScreenPointToRay(InputHandler.GetCursorPosition(), Camera.MonoOrStereoscopicEye.Mono), out hit, 1000f, ~0))
        {
            Vector3 newGhostPosition = hit.point.ToGridPositionAsVector();

            if (newGhostPosition != cachedGhostPosition)
            {
                GhostObject.transform.position = newGhostPosition;
                hoveredStructure = StructureGrid.GetStructureAtPosition(newGhostPosition);
            }

            cachedGhostPosition = newGhostPosition;
        }

        bool readyToRemoveStructure = (hoveredStructure != null);

        if (readyToRemoveStructure)
        {
            if (InputHandler.GetSelectInput_Down())
            {
                if (hoveredStructure != null)
                    TryRemoveStructure(hoveredStructure);
            }
        }
    }

    [Client]
    private void TryRemoveStructure(StructureInstance hoveredStructure)
    {
        CmdTryRemoveStructure(hoveredStructure);
    }

    [Command]
    private void CmdTryRemoveStructure(StructureInstance hoveredStructure)
    {
        NetworkServer.UnSpawn(hoveredStructure.gameObject);
        Destroy(hoveredStructure.gameObject);
        GlobalServerController.RebakeLevelNavmesh();
    }

    private void OnDisable()
    {
        GhostObject.SetActive(false);
    }
}
