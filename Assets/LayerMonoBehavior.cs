﻿using System.Linq;
using UnityEngine;

abstract class LayerMonoBehavior : MonoBehaviour, ILayerSetter
{
    public virtual int Layer => -1;

    private void OnValidate()
    {
        if (GetComponents<ILayerSetter>()?.Any(x => x.Layer != this.Layer) ?? false)
            Debug.LogWarning($"{gameObject.name} contains two or more conflicting implementations of ILayerSetter. Make sure all ILayerSetter components refer to the same layer.");

        if (gameObject.layer != Layer)
            gameObject.layer = Layer;
    }
}