﻿using Mirror;
using UnityEngine;

[RequireComponent(typeof(ItemInstance))]
public class AutoPickupable : NetworkBehaviour
{
    private ItemInstance ItemInstance => (itemInstance != null) ? itemInstance : (itemInstance = GetComponent<ItemInstance>());
    private ItemInstance itemInstance;

    internal void OnTryPickUp(FpsPlayerController obj)
    {
        CmdOnTryPickUp(obj.netIdentity);
    }

    [Command(requiresAuthority = false)]
    private void CmdOnTryPickUp(NetworkIdentity netIdentity)
    {
        NetworkServer.UnSpawn(gameObject);
    }
}