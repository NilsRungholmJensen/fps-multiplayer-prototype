﻿using Mirror;
using System.Collections.Generic;
using UnityEngine;

internal class ConstructionConsole : NetworkBehaviour
{
    private Interactable[] Interactables => (interactables != null) ? interactables : (interactables = GetComponentsInChildren<Interactable>());
    private Interactable[] interactables;

    private void OnEnable()
    {
        foreach (Interactable intr in Interactables)
        {
            intr.onInteraction += OnInteraction;
        }
    }

    private void OnDisable()
    {
        foreach (Interactable intr in Interactables)
        {
            intr.onInteraction -= OnInteraction;
        }
    }

    private void OnInteraction(FpsPlayerController player)
    {
        player.TryTransitionToBuilderPlayer();
    }
}