﻿using UnityEngine;

internal static class StructureGridModel
{
    internal static readonly float cellSize = 2.5f;
    internal static readonly int gridHalfSizeX = 1024;
    internal static readonly int gridHalfSizeZ = 1024;
    internal static readonly float groundLevel = 0f;

    private static readonly (int, int)[] gridRotationsAsVectors = new (int, int)[] { (0,1), (-1, 0), (0, -1), (1, 0) };
    #region Extension methods

    public static Vector3 ToWorldPosition(this (int, int) gridPosition)
    {
        return new Vector3((gridPosition.Item1 - gridHalfSizeX) * cellSize, groundLevel, (gridPosition.Item2 - gridHalfSizeZ) * cellSize);
    }

    public static Vector3 ToGridPositionAsVector(this Vector3 worldPosition)
    {
        return worldPosition.ToGridPosition().ToWorldPosition();
    }

    public static (int, int) ToGridPosition(this Vector3 worldPosition)
    {
        return (Mathf.RoundToInt(worldPosition.x / cellSize) + gridHalfSizeX, Mathf.RoundToInt(worldPosition.z / cellSize) + gridHalfSizeZ);
    }

    public static GridRotation ToGridRotation(this Quaternion quaternion)
    {
        return (GridRotation)Mathf.RoundToInt(quaternion.eulerAngles.y / 90f);
    }

    public static Quaternion ToQuaternion(this GridRotation rotation)
    {
        return Quaternion.Euler(0f, (int)rotation * 90f, 0f);
    }

    public static (int, int) Offset(this (int, int) originalPosition, GridRotation rotation)
    {
        (int, int) offset = rotation.ToPositionOffset();

        return ((originalPosition.Item1 + offset.Item1), (originalPosition.Item2 + offset.Item2));
    }

    public static (int, int) ToPositionOffset(this GridRotation rotation)
    {
        return gridRotationsAsVectors[(int)rotation];
    }

    #endregion
}