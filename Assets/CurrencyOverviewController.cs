﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CurrencyOverviewController : MonoBehaviour, IBuilderPlayerDependant
{
    [SerializeField] private TextMeshProUGUI totalCurrencyTextField;
    [SerializeField] private TextMeshProUGUI selectedStructureCostTextField;

    private BuilderPlayerController playerController;
    private PlaceStructureFeature placeStructureFeature;

    public Action<BuilderPlayerController> OnStartPlayerAuthority => StartAuthority;
    public Action<BuilderPlayerController> OnStopPlayerAuthority => StopAuthority;

    private void StartAuthority(BuilderPlayerController playerController)
    {
        this.playerController = playerController;
        this.placeStructureFeature = playerController.GetComponent<PlaceStructureFeature>();
        this.enabled = (placeStructureFeature != null);
    }

    private void StopAuthority(BuilderPlayerController playerController)
    {
        this.enabled = false;
    }

    private void Update()
    {
        totalCurrencyTextField.text = (0).ToString();
        selectedStructureCostTextField.text = placeStructureFeature?.SelectedStructure?.price.ToString() ?? "-";
    }
}
