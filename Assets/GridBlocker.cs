﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridBlocker : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnDrawGizmosSelected()
    {
        float gridSize = StructureGrid.CellSize;
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position.ToGridPositionAsVector() + (Vector3.up * gridSize * 0.05f), new Vector3(gridSize, gridSize * 0.1f, gridSize));
    }
}
