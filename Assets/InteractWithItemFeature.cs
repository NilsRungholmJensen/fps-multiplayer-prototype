﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Feature component, allowing a player to interact with objects containing an Interactable
/// </summary>
public class InteractWithItemFeature : FpsPlayerFeature
{
    [SerializeField] internal float maxInteractionDistance = 2f;
    internal Interactable interactableUnderCrosshair = null;
    internal Action<Interactable> onInteractableHovered;
    internal Action onNoInteractableHovered;

    private void Update()
    {
        int layerMask = 1 << LayerMask.NameToLayer("Interactable");
        RaycastHit hit;
       
        if (Physics.Raycast(PlayerController.AimOrigin, PlayerController.AimDirection, out hit, maxInteractionDistance, layerMask))
        {
            interactableUnderCrosshair = hit.collider.GetComponentInParent<Interactable>();
            onInteractableHovered?.Invoke(interactableUnderCrosshair);
        }
        else
        {
            interactableUnderCrosshair = null;
            onNoInteractableHovered?.Invoke();
        }

        if (InputHandler.GetInteractInput_Down())
        {
            if (!interactableUnderCrosshair)
                return;

            interactableUnderCrosshair.OnInteraction(this.PlayerController);
        }
    }
}
