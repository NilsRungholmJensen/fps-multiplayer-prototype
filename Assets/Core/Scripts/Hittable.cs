﻿using Mirror;
using System;
using UnityEngine;
using UnityEngine.VFX;

/// <summary>
/// Indicates that this GameObject can be hit by bullets and projectiles
/// </summary>
internal class Hittable : MonoBehaviour
{
    [SerializeField] internal bool playEffectOnHit;
    [SerializeField] internal VisualEffect effectToPlay;
    
    internal Action<float> OnHit;
    
    internal void Hit(Vector3 position, Vector3 impact, float damage)
    {
        if (playEffectOnHit)
        {
            Transform effectTransform = effectToPlay.transform;
            
            effectTransform.position = position;
            effectToPlay.transform.LookAt(effectTransform.position - impact.normalized);
            effectToPlay.Play();
        }
        
        OnHit?.Invoke(damage);
    }
}