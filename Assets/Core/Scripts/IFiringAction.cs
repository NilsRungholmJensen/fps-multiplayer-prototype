﻿using UnityEngine;

internal interface IFiringAction
{
    /// <summary>
    /// Attempt to fire the FiringAction by the Fire-button being pressed down once. Returns {true} on success.
    /// </summary>
    bool TryFireDown();

    /// <summary>
    /// Attempt to fire the FiringAction by the Fire-button being held down. Returns {true} on success.
    /// </summary>
    /// <returns></returns>
    bool TryFireHeld();
}

internal abstract class BaseFiringAction
{
    protected WeaponBlueprint blueprint;
    protected float timeOfLastShot = -100;

    internal BaseFiringAction(WeaponBlueprint _blueprint)
    {
        blueprint = _blueprint;
    }
}

internal class SemiautomaticAction : BaseFiringAction, IFiringAction
{
    internal SemiautomaticAction(WeaponBlueprint _blueprint) : base(_blueprint) { }

    public bool TryFireDown()
    {
        if ((Time.time - timeOfLastShot) < blueprint.selfLoadTime)
        { return false; }

        timeOfLastShot = Time.time;
        return true;
    }

    public bool TryFireHeld()
    {
        return false;
    }
}

internal class BurstAction : BaseFiringAction, IFiringAction
{
    private int roundsFiredInBurst = 0;

    internal BurstAction(WeaponBlueprint _blueprint) : base(_blueprint) { }

    public bool TryFireDown()
    {
        //Starting a new burst
        if ((Time.time - timeOfLastShot) < blueprint.burstLoadTime)
        { return false; }

        roundsFiredInBurst = 1;
        timeOfLastShot = Time.time;
        return true;
    }

    public bool TryFireHeld()
    {
        //Still in the middle of a burst
        if (roundsFiredInBurst < blueprint.roundsPerBurst)
        {
            if ((Time.time - timeOfLastShot) < blueprint.selfLoadTime)
            { return false; }

            roundsFiredInBurst++;
            timeOfLastShot = Time.time;
            return true;
        }

        //Otherwise we can't fire
        return false;
    }
}

internal class AutomaticAction : BaseFiringAction, IFiringAction
{
    internal AutomaticAction(WeaponBlueprint _blueprint) : base(_blueprint) { }

    public bool TryFireDown()
    {
        if ((Time.time - timeOfLastShot) < blueprint.selfLoadTime)
        { return false; }

        timeOfLastShot = Time.time;
        return true;
    }

    public bool TryFireHeld()
    {
        if ((Time.time - timeOfLastShot) < blueprint.selfLoadTime)
        { return false; }

        timeOfLastShot = Time.time;
        return true;
    }
}