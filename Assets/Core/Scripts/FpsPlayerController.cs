﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(FpsInputProvider), typeof(CharacterController))]
public class FpsPlayerController : AbstractPlayerController, IFpsPlayerDependant
{
    [Header("General")]
    [Tooltip("Force applied downward when in the air")]
    public float gravityDownForce = 20f;
    [Tooltip("Physic layers checked to consider the player grounded")]
    public LayerMask groundCheckLayers = -1;
    [Tooltip("distance from the bottom of the character controller capsule to test for grounded")]
    private const float groundCheckDistance = 0.07f;

    [Header("Movement")]
    [Tooltip("Max movement speed when grounded (when not sprinting)")]
    public float maxSpeedOnGround = 10f;
    [Tooltip("Sharpness for the movement when grounded, a low value will make the player accelerate and decelerate slowly, a high value will do the opposite")]
    public float movementSharpnessOnGround = 15;
    [Tooltip("Max movement speed when crouching")]
    [Range(0, 1)]
    public float maxSpeedCrouchedRatio = 0.5f;
    [Tooltip("Max movement speed when not grounded")]
    public float maxSpeedInAir = 10f;
    [Tooltip("Acceleration speed when in the air")]
    public float accelerationSpeedInAir = 25f;
    [Tooltip("Multiplicator for the sprint speed (based on grounded speed)")]
    public float sprintSpeedModifier = 2f;
    [Tooltip("Height at which the player dies instantly when falling off the map")]
    public float killHeight = -50f;

    [Header("Rotation")]
    [Tooltip("Rotation speed for moving the camera")]
    public float rotationSpeed = 200f;
    [Range(0.1f, 1f)]
    [Tooltip("Rotation speed multiplier when aiming")]
    public float aimingRotationMultiplier = 0.4f;

    [Header("Jump")]
    [Tooltip("Force applied upward when jumping")]
    public float jumpForce = 9f;

    [Header("Stance")]
    [Tooltip("Ratio (0-1) of the character height where the camera will be at")]
    public float cameraHeightRatio = 0.9f;

    [Tooltip("Height of character when standing")]
    public float capsuleHeightStanding = 1.8f;
    [Tooltip("Height of character when crouching")]
    public float capsuleHeightCrouching = 0.9f;
    [Tooltip("Speed of crouching transitions")]
    public float crouchingSharpness = 10f;

    [Header("Audio")]
    [Tooltip("Amount of footstep sounds played when moving one meter")]
    public float footstepSFXFrequency = 1f;
    [Tooltip("Amount of footstep sounds played when moving one meter while sprinting")]
    public float footstepSFXFrequencyWhileSprinting = 1f;
    [Tooltip("Sound played for footsteps")]
    public AudioClip footstepSFX;

    internal void SetFOV(float v)
    {
        throw new NotImplementedException();
    }

    [Tooltip("Sound played when jumping")]
    public AudioClip jumpSFX;
    [Tooltip("Sound played when landing")]
    public AudioClip landSFX;
    [Tooltip("Sound played when taking damage froma fall")]
    public AudioClip fallDamageSFX;

    [Header("Fall Damage")]
    [Tooltip("Whether the player will recieve damage when hitting the ground at high speed")]
    public bool recievesFallDamage;

    [Tooltip("Minimun fall speed for recieving fall damage")]
    public float minSpeedForFallDamage = 10f;
    [Tooltip("Fall speed for recieving th emaximum amount of fall damage")]
    public float maxSpeedForFallDamage = 30f;
    [Tooltip("Damage recieved when falling at the mimimum speed")]
    public float fallDamageAtMinSpeed = 10f;
    [Tooltip("Damage recieved when falling at the maximum speed")]
    public float fallDamageAtMaxSpeed = 50f;

    protected IFpsInputHandler InputHandler => (inputHandler != null) ? inputHandler : (inputHandler = GetComponent<FpsInputProvider>().Handler);
    protected IFpsInputHandler inputHandler;

    CharacterController CharacterController => characterController ? characterController : (characterController = GetComponent<CharacterController>());
    private CharacterController characterController;

    internal Vector3 AimOrigin => playerCamera?.transform.position ?? Vector3.zero;
    internal Vector3 AimDirection => playerCamera?.transform.forward ?? Vector3.down;

    private float verticalRotation;
    private bool isGrounded;
    public bool hasJumpedThisFrame { get; private set; }
    private bool isCrouching;

    private bool locked = false;

    Vector3 groundNormal;
    public Vector3 characterVelocity { get; set; }
    
    Vector3 m_LatestImpactSpeed;
    float m_LastTimeJumped = 0f;
    float m_CameraVerticalAngle = 0f;
    float m_footstepDistanceCounter;
    float m_TargetCharacterHeight;
    private const float defaultFieldOfView = 75f;
    private const float k_JumpGroundingPreventionTime = 0.2f;
    private const float k_GroundCheckDistanceInAir = 0.07f;

    public Action<FpsPlayerController> OnStartPlayerAuthority => StartAuthority;
    public Action<FpsPlayerController> OnStopPlayerAuthority => StopAuthority;

    public void Start()
    {
        
    }

    public override void OnStartAuthority()
    {
        if (!hasAuthority)
            return;

        //Debug.Log("Fps for player with netId " + netId + ": START authority!");
        //Place main camera in this transform
        playerCamera = Camera.main;
        Camera.main.transform.localPosition = new Vector3(0, 0, 0);
        Camera.main.transform.localRotation = Quaternion.identity;
        Camera.main.fieldOfView = defaultFieldOfView;

        playerCamera.transform.SetParent(gameObject.transform, false);

        //Lock cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        SetCrouchingState(false, false);
        UpdateCharacterHeight(true);

        foreach (IFpsPlayerDependant playerDependant in UnityEngineExtensions.FindAllComponentsInScene().OfType<IFpsPlayerDependant>())
        {
            playerDependant.OnStartPlayerAuthority?.Invoke(this);
        }
    }

    public override void OnStopAuthority()
    {
        if (hasAuthority)
            return;

        //If player camera is still present as child..
        if (playerCamera != null && playerCamera.transform.parent == this.transform)
        {
            playerCamera.transform.SetParent(null, false);

            //Return main camera to root transform

            playerCamera.transform.localPosition = new Vector3(0, 0, 0);
            playerCamera.transform.localRotation = Quaternion.identity;
            playerCamera.fieldOfView = defaultFieldOfView;

            playerCamera = null;
        }

        foreach (IFpsPlayerDependant playerDependant in UnityEngineExtensions.FindAllComponentsInScene().OfType<IFpsPlayerDependant>())
        {
            playerDependant.OnStopPlayerAuthority?.Invoke(this);
        }

        //Debug.Log("Fps for player with netId " + netId + ": STOP authority!");
    }

    //Used when this instance (as an IFpsPlayerDependant) is informed about authority change (triggered by OnStartAuthority)
    private void StartAuthority(AbstractPlayerController playerController)
    {
        GetComponent<FpsInputProvider>().Lock(false);
        locked = false;
    }

    //Used when this instance (as an IFpsPlayerDependant) is informed about authority change (triggered by the OnStopAuthority)
    private void StopAuthority(AbstractPlayerController playerController)
    {
        GetComponent<FpsInputProvider>().Lock(true);
        locked = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer) return;

        //--- Always executed for local player ---

        hasJumpedThisFrame = false;
        bool wasGrounded = isGrounded;

        HandleGroundedState();
        UpdateCharacterHeight(false);

        //--- Only executed when the character is unlocked ---

        if (locked)
            return;

        HandleTurnAndLook();
        HandleMovement();
    }

    void HandleGroundedState()
    {
        // Make sure that the ground check distance while already in air is very small, to prevent suddenly snapping to ground
        float chosenGroundCheckDistance = isGrounded ? (CharacterController.skinWidth + groundCheckDistance) : k_GroundCheckDistanceInAir;

        // reset values before the ground check
        isGrounded = false;
        groundNormal = Vector3.up;

        // only try to detect ground if it's been a short amount of time since last jump; otherwise we may snap to the ground instantly after we try jumping
        //if (Time.time < m_LastTimeJumped + k_JumpGroundingPreventionTime)
        {
            //return;
        }

        // if we're grounded, collect info about the ground normal with a downward capsule cast representing our character capsule
        var capsuleCast = Physics.CapsuleCast(GetCapsuleBottomHemisphere(), GetCapsuleTopHemisphere(CharacterController.height), CharacterController.radius, Vector3.down, out RaycastHit hit, chosenGroundCheckDistance, groundCheckLayers, QueryTriggerInteraction.Ignore);
        if (capsuleCast)
        {
            // storing the upward direction for the surface found
            groundNormal = hit.normal;

            // Only consider this a valid ground hit if the ground normal goes in the same direction as the character up
            // and if the slope angle is lower than the character controller's limit
            if (Vector3.Dot(hit.normal, transform.up) > 0f &&
                IsNormalUnderSlopeLimit(groundNormal))
            {
                isGrounded = true;

                // handle snapping to the ground
                if (hit.distance > CharacterController.skinWidth)
                {
                    CharacterController.Move(Vector3.down * hit.distance);
                }
            }
        }
    }

    void HandleMovement()
    {
        // character movement handling

        Vector2 moveInput = InputHandler.GetMovementInput();
        // converts move input to a worldspace vector based on our character's transform orientation
        Vector3 worldspaceMoveInput = transform.TransformVector(new Vector3(moveInput.x, 0f, moveInput.y));

        float sprintSpeedModifier = 1f; //For sprinting, crouching etc.

        // handle grounded movement
        if (isGrounded)
        {
            // calculate the desired velocity from inputs, max speed, and current slope
            Vector3 targetVelocity = worldspaceMoveInput * maxSpeedOnGround * sprintSpeedModifier;
            // reduce speed if crouching by crouch speed ratio
            if (isCrouching)
                targetVelocity *= maxSpeedCrouchedRatio;
            targetVelocity = GetDirectionReorientedOnSlope(targetVelocity.normalized, groundNormal) * targetVelocity.magnitude;

            // smoothly interpolate between our current velocity and the target velocity based on acceleration speed
            characterVelocity = Vector3.Lerp(characterVelocity, targetVelocity, movementSharpnessOnGround * Time.deltaTime);

            
            // jumping
            if (isGrounded && (InputHandler.GetJumpInput_Down()))
            {
                
                // force the crouch state to false
                //if (SetCrouchingState(false, false))
                {
                    // start by canceling out the vertical component of our velocity
                    characterVelocity = new Vector3(characterVelocity.x, 0f, characterVelocity.z);

                    // then, add the jumpSpeed value upwards
                    characterVelocity += Vector3.up * jumpForce;

                    // remember last time we jumped because we need to prevent snapping to ground for a short time
                    m_LastTimeJumped = Time.time;

                    hasJumpedThisFrame = true;

                    // Force grounding to false
                    isGrounded = false;
                    groundNormal = Vector3.up;
                }
            }
            

            /*
            // footsteps sound
            float chosenFootstepSFXFrequency = (isSprinting ? footstepSFXFrequencyWhileSprinting : footstepSFXFrequency);
            if (m_footstepDistanceCounter >= 1f / chosenFootstepSFXFrequency)
            {
                m_footstepDistanceCounter = 0f;
                audioSource.PlayOneShot(footstepSFX);
            }
            
            // keep track of distance traveled for footsteps sound
            m_footstepDistanceCounter += characterVelocity.magnitude * Time.deltaTime;
            */
        }
        // handle air movement
        else
        {
            // add air acceleration
            characterVelocity += worldspaceMoveInput * accelerationSpeedInAir * Time.deltaTime;

            // limit air speed to a maximum, but only horizontally
            float verticalVelocity = characterVelocity.y;
            Vector3 horizontalVelocity = Vector3.ProjectOnPlane(characterVelocity, Vector3.up);
            horizontalVelocity = Vector3.ClampMagnitude(horizontalVelocity, maxSpeedInAir * sprintSpeedModifier);
            characterVelocity = horizontalVelocity + (Vector3.up * verticalVelocity);

            // apply the gravity to the velocity
            characterVelocity += Vector3.down * gravityDownForce * Time.deltaTime;
        }


        // apply the final calculated velocity value as a character movement
        Vector3 capsuleBottomBeforeMove = GetCapsuleBottomHemisphere();
        Vector3 capsuleTopBeforeMove = GetCapsuleTopHemisphere(CharacterController.height);
        CharacterController.Move(characterVelocity * Time.deltaTime);

        // detect obstructions to adjust velocity accordingly
        m_LatestImpactSpeed = Vector3.zero;
        if (Physics.CapsuleCast(capsuleBottomBeforeMove, capsuleTopBeforeMove, CharacterController.radius, characterVelocity.normalized, out RaycastHit hit, characterVelocity.magnitude * Time.deltaTime, -1, QueryTriggerInteraction.Ignore))
        {
            // We remember the last impact speed because the fall damage logic might need it
            m_LatestImpactSpeed = characterVelocity;

            characterVelocity = Vector3.ProjectOnPlane(characterVelocity, hit.normal);
        }
    }

    private void HandleTurnAndLook()
    {
        Vector2 lookInput = InputHandler.GetLookInput();

        // horizontal character rotation
        {
            // rotate the transform with the input speed around its local Y axis
            transform.Rotate(new Vector3(0f, lookInput.x * rotationSpeed, 0f), Space.Self);
        }

        // vertical camera rotation
        {
            // add vertical inputs to the camera's vertical angle
            verticalRotation = lookInput.y * rotationSpeed;

            // limit the camera's vertical angle to min/max
            float targetCameraVerticalAngle = Mathf.Clamp(verticalRotation, -89f, 89f);

            // apply the vertical angle as a local rotation to the camera transform along its right axis (makes it pivot up and down)
            if (playerCamera)
            { playerCamera.transform.Rotate(new Vector3(verticalRotation, 0, 0), Space.Self); }
        }
    }

    // Gets a reoriented direction that is tangent to a given slope
    public Vector3 GetDirectionReorientedOnSlope(Vector3 direction, Vector3 slopeNormal)
    {
        Vector3 directionRight = Vector3.Cross(direction, transform.up);
        return Vector3.Cross(slopeNormal, directionRight).normalized;
    }

    // Gets the center point of the bottom hemisphere of the character controller capsule    
    Vector3 GetCapsuleBottomHemisphere()
    {
        return transform.position + (transform.up * CharacterController.radius);
    }

    // Gets the center point of the top hemisphere of the character controller capsule    
    Vector3 GetCapsuleTopHemisphere(float atHeight)
    {
        return transform.position + (transform.up * (atHeight - CharacterController.radius));
    }

    // Returns true if the slope angle represented by the given normal is under the slope angle limit of the character controller
    bool IsNormalUnderSlopeLimit(Vector3 normal)
    {
        return Vector3.Angle(transform.up, normal) <= CharacterController.slopeLimit;
    }
    void UpdateCharacterHeight(bool force)
    {
        // Update height instantly
        if (force)
        {
            CharacterController.height = m_TargetCharacterHeight;
            CharacterController.center = Vector3.up * CharacterController.height * 0.5f;
            playerCamera.transform.localPosition = Vector3.up * m_TargetCharacterHeight * cameraHeightRatio;
            //m_Actor.aimPoint.transform.localPosition = m_Controller.center;
        }
        // Update smooth height
        else if (CharacterController.height != m_TargetCharacterHeight)
        {
            // resize the capsule and adjust camera position
            CharacterController.height = Mathf.Lerp(CharacterController.height, m_TargetCharacterHeight, crouchingSharpness * Time.deltaTime);
            CharacterController.center = Vector3.up * CharacterController.height * 0.5f;
            playerCamera.transform.localPosition = Vector3.Lerp(playerCamera.transform.localPosition, Vector3.up * m_TargetCharacterHeight * cameraHeightRatio, crouchingSharpness * Time.deltaTime);
            //m_Actor.aimPoint.transform.localPosition = m_Controller.center;
        }
    }

    // returns false if there was an obstruction
    bool SetCrouchingState(bool crouched, bool ignoreObstructions)
    {
        // set appropriate heights
        if (crouched)
        {
            m_TargetCharacterHeight = capsuleHeightCrouching;
        }
        else
        {
            // Detect obstructions
            if (!ignoreObstructions)
            {
                Collider[] standingOverlaps = Physics.OverlapCapsule(
                    GetCapsuleBottomHemisphere(),
                    GetCapsuleTopHemisphere(capsuleHeightStanding),
                    CharacterController.radius,
                    -1,
                    QueryTriggerInteraction.Ignore);
                foreach (Collider c in standingOverlaps)
                {
                    if (c != CharacterController)
                    {
                        return false;
                    }
                }
            }

            m_TargetCharacterHeight = capsuleHeightStanding;
        }

        isCrouching = crouched;
        return true;
    }

    internal void LerpTowardsCameraFov(float fovMultiplier, float lerpingSpeed)
    {
        if (playerCamera)
            playerCamera.fieldOfView = Mathf.Lerp(playerCamera.fieldOfView, defaultFieldOfView * fovMultiplier, lerpingSpeed * Time.deltaTime);
    }

    [Client]
    internal void TryTransitionToBuilderPlayer()
    {
        CmdTryTransitionToBuilderPlayer();
    }

    [Command]
    internal void CmdTryTransitionToBuilderPlayer()
    {
        GlobalServerController.TransitionToBuilderPlayer(this.netIdentity);
    }
}
