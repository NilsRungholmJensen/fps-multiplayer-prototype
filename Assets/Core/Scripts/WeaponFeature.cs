﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Component responsible for the feature of holding, aiming and firing the
/// weapons provided by the WeaponInventory component. Also applies recoil.
/// </summary>
[RequireComponent(typeof(WeaponInventory))]
public class WeaponFeature : FpsPlayerFeature
{
    [Header("References")]
    [Tooltip("Secondary camera used to avoid seeing weapon go throw geometries")]
    public Camera weaponCamera;
    [Tooltip("Parent transform where all weapon will be added in the hierarchy")]
    public Transform weaponSocket;
    [Tooltip("Position for weapons when active but not aiming")]
    public Transform defaultWeaponTransform;
    [Tooltip("Pose for weapons when aiming")]
    public Transform aimingWeaponTransform;
    [Tooltip("Position for innactive weapons")]
    public Transform downWeaponTransform;

    [Header("Misc")]
    [Tooltip("Field of view when not aiming")]
    public float defaultFOV = 60f;
    [Tooltip("Layer to set FPS weapon gameObjects to")]
    public LayerMask FPSWeaponLayer;

    //Speed at which the aiming animation is played
    public const float aimingAnimationSpeed = 10f;
    //Portion of the regular FOV to apply to the weapon camera
    public const float weaponFOVMultiplier = 0.8f;
    //Delay before switching weapon a second time, to avoid recieving multiple inputs from mouse wheel
    public float weaponSwitchDelay = 1f;

    private WeaponInventory WeaponInventory => (weaponInventory != null) ? weaponInventory : (weaponInventory = GetComponent<WeaponInventory>());
    private WeaponInventory weaponInventory;
    private WeaponInstance Weapon => WeaponInventory.EquippedWeapon;

    //private GameObject WeaponModel => (weaponModel != null) ? weaponModel : (weaponModel = CreateWeaponModel());
    private GameObject weaponModel;
    private Vector3 accumulatedModelRecoil;

    private float baseFov, recoilFov;
    private Vector3 weaponBasePosition, weaponRecoilPosition, cameraRecoilPosition;
    private Quaternion weaponBaseRotation, weaponRecoilRotation;

    private const float modelRecoilSharpness = 80f;
    private const float modelRecoilRestitutionSharpness = 60f;
    private const float cameraRecoilSharpness = 150f;
    private const float maxRecoilDistance = 0.1f;

    void EquipWeapon(WeaponInstance weapon)
    {
        if (weaponModel != null)
            Destroy(weaponModel);

        weaponModel = InstantiateWeaponModel(weapon.modelPrefab);
    }

    GameObject InstantiateWeaponModel(GameObject original)
    {
        GameObject spawnedModel = GameObject.Instantiate(original, weaponSocket);
        spawnedModel.transform.localPosition = Vector3.zero;
        spawnedModel.transform.rotation = Quaternion.identity;

        return spawnedModel;
    }

    private void Start()
    {
        EquipWeapon(Weapon);
    }

    // Update is called once per frame
    void Update()
    {
        //Firing
        Vector3 aimOrigin = PlayerController.AimOrigin;
        Vector3 aimDirection = PlayerController.AimDirection;

        bool didFire = false;

        if (InputHandler.GetFireInput_Down())
        {
            didFire = (Weapon?.TryFireDown(aimOrigin, aimDirection) ?? false) || didFire;
        }

        if (InputHandler.GetFireInput_Held())
        {
            didFire = (Weapon?.TryFireHeld(aimOrigin, aimDirection) ?? false) || didFire;
        }

        // Handle accumulating recoil
        if (didFire)
        {
            accumulatedModelRecoil += Vector3.back * Weapon.modelRecoil;
            cameraRecoilPosition += new Vector3(
                -Weapon.cameraRecoil,
                UnityEngine.Random.Range(-Weapon.cameraRecoil * 0.3f, Weapon.cameraRecoil * 0.3f),
                0f);
        }

        //Get all combining positions and rotations
        GetWeaponBasePose(InputHandler.GetAimInput_Held(), out weaponBasePosition, out weaponBaseRotation, out baseFov);

        UpdateModelRecoil();

        UpdateCameraRecoil();

        //Apply all combining positions and rotations
        LerpTowardsCameraFov(baseFov + recoilFov);

        weaponSocket.transform.localPosition = weaponBasePosition + weaponRecoilPosition;
        weaponSocket.transform.localRotation = weaponBaseRotation * weaponRecoilRotation;

        //Make weaponCamera follow playerCamera
        if (PlayerController?.playerCamera)
        {
            weaponCamera.transform.position = PlayerController.playerCamera.transform.position;
            weaponCamera.transform.rotation = PlayerController.playerCamera.transform.rotation;
        }
    }

    private void UpdateCameraRecoil()
    {
        Transform cameraTransform = PlayerController?.playerCamera?.transform;
        if (!cameraTransform)
            return;

        Vector3 appliedRecoil = Vector3.Lerp(Vector3.zero, cameraRecoilPosition, cameraRecoilSharpness * Time.deltaTime);

        cameraTransform.Rotate(appliedRecoil, Space.Self);
        cameraRecoilPosition -= appliedRecoil;
    }

    private void UpdateModelRecoil()
    {
        accumulatedModelRecoil = Vector3.ClampMagnitude(accumulatedModelRecoil, maxRecoilDistance);

        // if the accumulated recoil is further away from the current position, make the current position move towards the recoil target
        if (weaponRecoilPosition.z >= accumulatedModelRecoil.z * 0.99f)
        {
            
            weaponRecoilPosition = Vector3.Lerp(weaponRecoilPosition, accumulatedModelRecoil, modelRecoilSharpness * Time.deltaTime);
        }
        // otherwise, move recoil position to make it recover towards its resting pose
        else
        {
            weaponRecoilPosition = Vector3.Lerp(weaponRecoilPosition, Vector3.zero, modelRecoilRestitutionSharpness * Time.deltaTime);
            accumulatedModelRecoil = weaponRecoilPosition;
        }

        recoilFov = accumulatedModelRecoil.magnitude * 0.5f;
    }

    // Updates weapon position and camera FoV for the aiming transition
    void GetWeaponBasePose(bool isAiming, out Vector3 targetPosition, out Quaternion targetRotation, out float targetFov)
    {
        Vector3 targetWeaponLocalPosition;
        Quaternion targetWeaponLocalRotation;

        if (isAiming)
        {
            targetWeaponLocalPosition = aimingWeaponTransform.localPosition + Weapon.aimOffset;
            targetWeaponLocalRotation = aimingWeaponTransform.localRotation;
            targetFov = Weapon.aimZoomRatio;
        }
        else
        {
            targetWeaponLocalPosition = defaultWeaponTransform.localPosition;
            targetWeaponLocalRotation = defaultWeaponTransform.localRotation;
            targetFov = 1f;
        }

        targetPosition = Vector3.Lerp(weaponSocket.transform.localPosition, targetWeaponLocalPosition, aimingAnimationSpeed * Time.deltaTime);
        targetRotation = Quaternion.Lerp(weaponSocket.transform.localRotation, targetWeaponLocalRotation, aimingAnimationSpeed * Time.deltaTime);
    }

    void LerpTowardsCameraFov(float fovMultiplier)
    {
        PlayerController.LerpTowardsCameraFov(fovMultiplier, aimingAnimationSpeed);
    }
}
