﻿using Mirror;
using System;
using UnityEngine;
using static WeaponBlueprint;

internal class WeaponInstance
{
    internal string displayName => blueprint.displayName;
    internal GameObject modelPrefab => blueprint.modelPrefab;
    internal Vector3 aimOffset => blueprint.aimOffset;
    internal float aimZoomRatio => blueprint.aimZoomRatio;
    internal float cameraRecoil => blueprint.cameraRecoil;
    internal float modelRecoil => blueprint.modelRecoil;

    //Defines how the weapon works
    private readonly WeaponBlueprint blueprint;

    private readonly IFiringAction firingAction;
    private readonly IWeaponsChamber chamber;
    private float currentSpread = 0.1f;
    

    internal WeaponInstance(WeaponBlueprint _blueprint)
    {
        blueprint = _blueprint;
        //Factory
        firingAction = GetIFiringAction(_blueprint);
        chamber = GetIWeaponsChamber(_blueprint);
    }

    private IFiringAction GetIFiringAction(WeaponBlueprint blueprint)
    {
        switch (blueprint.firingActionType)
        {
            case FiringActionType.Semi:
            default:
                return new SemiautomaticAction(blueprint);
            case FiringActionType.Burst:
                return new BurstAction(blueprint);
            case FiringActionType.Auto:
                return new AutomaticAction(blueprint);
        }
    }

    private IWeaponsChamber GetIWeaponsChamber(WeaponBlueprint blueprint)
    {
        switch (blueprint.projectileType)
        {
            case ProjectileType.Instant:
            default:
                return new InstantWeaponsChamber(blueprint);
            case ProjectileType.Launched:
                throw new NotImplementedException();
        }
    }

    internal virtual bool TryFireDown(Vector3 aimOrigin, Vector3 aimDirection, MonoBehaviour spawningMonoBehaviour = null)
    {
        bool result = firingAction.TryFireDown();

        if (result)
        {
            chamber.CmdShoot(aimOrigin, aimDirection, currentSpread, spawningMonoBehaviour);
        }
        return result;
    }

    internal virtual bool TryFireHeld(Vector3 aimOrigin, Vector3 aimDirection, MonoBehaviour spawningMonoBehaviour = null)
    {
        bool result = firingAction.TryFireHeld();

        if (result)
        {
            chamber.CmdShoot(aimOrigin, aimDirection, currentSpread, spawningMonoBehaviour);
        }
        return result;
    }
}

internal interface IWeaponsChamber
{
    [Command]
    void CmdShoot(Vector3 shootingOrigin, Vector3 shootingDirection, float currentSpread, MonoBehaviour spawningMonoBehaviour = null);
}

internal abstract class BaseWeaponsChamber
{
    protected WeaponBlueprint blueprint;
    protected readonly int layersHitByShots = LayerMask.GetMask(new string[] { "Default", "Structure" });

    internal BaseWeaponsChamber(WeaponBlueprint _blueprint)
    {
        blueprint = _blueprint;
    }

    protected Vector3 GetShotDirectionWithinSpread(Vector3 shootingDirection, float currentSpread)
    {
        float spreadAngleRatio = currentSpread / 180f;
        Vector3 spreadWorldDirection = Vector3.Slerp(shootingDirection, UnityEngine.Random.insideUnitSphere, spreadAngleRatio);

        return spreadWorldDirection;
    }
}
internal class InstantWeaponsChamber : BaseWeaponsChamber, IWeaponsChamber
{
    internal InstantWeaponsChamber(WeaponBlueprint _blueprint) : base(_blueprint) { }

    [Command]
    public void CmdShoot(Vector3 shootingOrigin, Vector3 shootingDirection, float currentSpread, MonoBehaviour spawningMonoBehaviour = null)
    {
        for (int i = 0; i < blueprint.bulletsPerRound; i++)
        {
            RaycastHit hit;
            Vector3 shotDirection = GetShotDirectionWithinSpread(shootingDirection, currentSpread);

            if (Physics.Raycast(shootingOrigin, shootingDirection, out hit, 100f, layersHitByShots))
            {
                Hittable other = hit.collider.GetComponentInParent<Hittable>();
                if (other)
                {
                    other.Hit(hit.point, shootingDirection.normalized * 1500f, blueprint.damageMultiplier * 10);
                    
                    if (blueprint.debugHitEffect)
                        UnityEngine.Object.Instantiate(blueprint.debugHitEffect, hit.point, Quaternion.identity);
                }
            }
        }
    }
}