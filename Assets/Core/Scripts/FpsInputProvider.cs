﻿using UnityEngine;

internal class FpsInputProvider : MonoBehaviour
{
    internal IFpsInputHandler Handler => (handler != null) ? handler : (handler = FpsInputHandlerFactory.GetInputHandler());
    private IFpsInputHandler handler;

    internal void Lock(bool doLock)
    {
        handler = doLock ? FpsInputHandlerFactory.GetLockedHandler() : FpsInputHandlerFactory.GetInputHandler();
    }
}