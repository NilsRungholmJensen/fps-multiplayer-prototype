﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponInventory : NetworkBehaviour
{
    [Header("DEBUG")]
    [SerializeField]
    private WeaponBlueprint startingWeaponDefinition;

    internal WeaponInstance EquippedWeapon => (equippedWeapon != null) ? equippedWeapon : (equippedWeapon = new WeaponInstance(startingWeaponDefinition));
    private WeaponInstance equippedWeapon;
}