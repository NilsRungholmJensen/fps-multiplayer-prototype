﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkModelHandler : NetworkBehaviour
{
    public GameObject foreignAppreance;
    public GameObject localAppeareance;

    public override void OnStartLocalPlayer()
    {
        if (localAppeareance) localAppeareance?.SetActive(isLocalPlayer);
        if (foreignAppreance) foreignAppreance?.SetActive(!isLocalPlayer);
    }
}
