﻿using System;

internal class FpsInputHandlerFactory
{
    internal static IFpsInputHandler GetInputHandler(bool locked = false)
    {
        return locked ? new LockedFpsInputHandler() as IFpsInputHandler : new MouseAndKeyboardFpsInputHandler() as IFpsInputHandler;
    }

    internal static IFpsInputHandler GetLockedHandler()
    {
        return new LockedFpsInputHandler() as IFpsInputHandler;
    }
}

internal class BuilderInputHandlerFactory
{
    internal static IBuilderInputHandler GetInputHandler(bool locked = false)
    {
        return locked ? new LockedBuilderInputHandler() as IBuilderInputHandler : new MouseAndKeyboardBuilderInputHandler() as IBuilderInputHandler;
    }

    internal static IBuilderInputHandler GetLockedHandler()
    {
        return new LockedBuilderInputHandler() as IBuilderInputHandler;
    }
}