﻿using UnityEngine;

internal class LockedFpsInputHandler : IFpsInputHandler
{
    public bool GetAimInput_Held()
    {
        return false;
    }

    public bool GetFireInput_Down()
    {
        return false;
    }

    public bool GetFireInput_Held()
    {
        return false;
    }

    public bool GetInteractInput_Down()
    {
        return false;
    }

    public bool GetInteractInput_Held()
    {
        return false;
    }

    public bool GetJumpInput_Down()
    {
        return false;
    }

    public Vector2 GetLookInput()
    {
        return Vector2.zero;
    }

    public Vector2 GetMovementInput()
    {
        return Vector2.zero;
    }
}

internal class LockedBuilderInputHandler : IBuilderInputHandler
{
    public bool GetAimInput_Held()
    {
        return false;
    }

    public bool GetAltSelectInput_Down()
    {
        return false;
    }

    public Vector2 GetCameraPanInput()
    {
        return Vector2.zero;
    }

    public float GetCameraZoomInput()
    {
        return 0f;
    }

    public Vector2 GetCursorPosition()
    {
        return Vector2.zero;
    }

    public bool GetExitBuilderModeInput_Down()
    {
        return false;
    }

    public bool GetExitBuilderModeInput_Held()
    {
        return false;
    }

    public bool GetFireInput_Down()
    {
        return false;
    }

    public bool GetFireInput_Held()
    {
        return false;
    }

    public bool GetInteractInput_Down()
    {
        return false;
    }

    public bool GetInteractInput_Held()
    {
        return false;
    }

    public bool GetJumpInput_Down()
    {
        return false;
    }

    public Vector2 GetLookInput()
    {
        return Vector2.zero;
    }

    public Vector2 GetMovementInput()
    {
        return Vector2.zero;
    }

    public bool GetSelectInput_Down()
    {
        return false;
    }

    public bool GetRotateStructureInput_Down()
    {
        return false;
    }
}