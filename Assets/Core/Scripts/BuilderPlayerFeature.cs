﻿using Mirror;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(BuilderInputProvider), typeof(BuilderPlayerController))]
public abstract class BuilderPlayerFeature : AbstractPlayerFeature, IBuilderPlayerDependant
{
    Action<BuilderPlayerController> IBuilderPlayerDependant.OnStartPlayerAuthority => base.StartAuthority;
    Action<BuilderPlayerController> IBuilderPlayerDependant.OnStopPlayerAuthority => base.StopAuthority;

    internal protected BuilderPlayerController PlayerController => (playerController != null) ? playerController : (playerController = GetComponent<BuilderPlayerController>());
    internal protected BuilderPlayerController playerController;

    internal protected IBuilderInputHandler InputHandler => (inputHandler != null) ? inputHandler : (inputHandler = GetComponent<BuilderInputProvider>().Handler);
    internal protected IBuilderInputHandler inputHandler;

    private protected bool isPointerOverUi()
    {
        return EventSystem.current.IsPointerOverGameObject();
    }

    private protected StructureInstance GetStructureAtGroundPosition(Vector3 ghostPosition)
    {
        Collider[] collidersHit = Physics.OverlapBox(ghostPosition + new Vector3(0f, StructureGrid.CellSize * 0.5f, 0f), Vector3.one * StructureGrid.CellSize * 0.49f);

        if (!collidersHit.Any())
            return null;

        StructureInstance[] structuresHit = collidersHit.Select(x => x.GetComponentInParent<StructureInstance>()).ToArray();

        if (!structuresHit.Any())
            return null;

        return structuresHit.FirstOrDefault();
    }

    /// <summary>
    /// Returns true if no actors/objects collide with a box corresponding to this cell. Used for checking if a cell is obstructed by a non-structure object.
    /// </summary>
    private protected bool GetIsCellVacant((int, int) position)
    {
        if (!StructureGrid.IsPositionFree(position))
            return false;

        Collider[] collidersHit = Physics.OverlapBox(position.ToWorldPosition(), Vector3.one * StructureGrid.CellSize * 0.6f, Quaternion.identity, LayerMask.GetMask(("")));

        return !collidersHit.Any();
    }

    /// <summary>
    /// Returns true if no actors/objects collide with *any* box corresponding to the provided cells. Used for checking if any of the cells are obstructed by a non-structure object.
    /// </summary>
    private protected bool GetAreAllCellsVacant((int, int)[] positions)
    {
        foreach ((int, int) pos in positions)
        {
            if (!GetIsCellVacant(pos))
            {
                return false;
            }
        }

        return true;
    }
}