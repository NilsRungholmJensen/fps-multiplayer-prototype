﻿using Mirror;
using System;
using UnityEngine;

[RequireComponent(typeof(FpsInputProvider), typeof(FpsPlayerController))]
public abstract class FpsPlayerFeature : AbstractPlayerFeature, IFpsPlayerDependant
{
    Action<FpsPlayerController> IFpsPlayerDependant.OnStartPlayerAuthority => base.StartAuthority;
    Action<FpsPlayerController> IFpsPlayerDependant.OnStopPlayerAuthority => base.StopAuthority;

    internal protected FpsPlayerController PlayerController => (playerController != null) ? playerController : (playerController = GetComponent<FpsPlayerController>());
    internal protected FpsPlayerController playerController;

    internal protected IFpsInputHandler InputHandler => (inputHandler != null) ? inputHandler : (inputHandler = GetComponent<FpsInputProvider>().Handler);
    internal protected IFpsInputHandler inputHandler;
}
