﻿using System;
using UnityEngine;

/// <summary>
/// Indicates that this GameObject can receive damage.
/// Requires Hittable component.
/// </summary>
[RequireComponent(typeof(Hittable))]
internal class Damagable : MonoBehaviour
{
    [SerializeField] private float damageMultiplier = 1.0f;
    internal Action<float> OnDamaged;

    private void OnEnable()
    {
        //Subscribe to hits
        GetComponent<Hittable>().OnHit += Damaged;
    }

    private void OnDisable()
    {
        //Remove subscription
        GetComponent<Hittable>().OnHit -= Damaged;
    }

    private void Damaged(float rawDamage)
    {
        OnDamaged?.Invoke(rawDamage * damageMultiplier);
    }
}