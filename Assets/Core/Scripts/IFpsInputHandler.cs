﻿using UnityEngine;

public interface IFpsInputHandler
{
    Vector2 GetMovementInput();
    Vector2 GetLookInput();
    bool GetJumpInput_Down();
    bool GetFireInput_Down();
    bool GetFireInput_Held();
    bool GetAimInput_Held();
    bool GetInteractInput_Down();
    bool GetInteractInput_Held();
}

public interface IBuilderInputHandler
{
    Vector2 GetCursorPosition();
    Vector2 GetCameraPanInput();
    float GetCameraZoomInput();
    //Vector2 GetLookInput();
    bool GetSelectInput_Down();
    //bool GetClickInput_Held();
    bool GetAltSelectInput_Down();
    bool GetExitBuilderModeInput_Down();
    bool GetExitBuilderModeInput_Held();
    bool GetRotateStructureInput_Down();
}