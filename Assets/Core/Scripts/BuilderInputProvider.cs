﻿using UnityEngine;

internal class BuilderInputProvider : MonoBehaviour
{
    internal IBuilderInputHandler Handler => (handler != null) ? handler : (handler = BuilderInputHandlerFactory.GetInputHandler());
    private IBuilderInputHandler handler;

    internal void Lock(bool doLock)
    {
        handler = doLock ? BuilderInputHandlerFactory.GetLockedHandler() : BuilderInputHandlerFactory.GetInputHandler();
    }
}