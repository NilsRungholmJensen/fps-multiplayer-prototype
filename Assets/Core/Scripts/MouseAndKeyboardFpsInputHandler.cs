﻿using UnityEngine;

internal class MouseAndKeyboardFpsInputHandler : IFpsInputHandler
{
    private float lookSensitivity;

    internal MouseAndKeyboardFpsInputHandler()
    {
        lookSensitivity = 1.0f;
    }

    public bool GetAimInput_Held()
    {
        return Input.GetButton(InputConstants.k_ButtonNameAim);
    }

    public bool GetFireInput_Down()
    {
        return Input.GetButtonDown(InputConstants.k_ButtonNameFire);
    }

    public bool GetFireInput_Held()
    {
        return Input.GetButton(InputConstants.k_ButtonNameFire);
    }

    public bool GetInteractInput_Down()
    {
        return Input.GetButtonDown(InputConstants.k_ButtonNameInteract);
    }

    public bool GetInteractInput_Held()
    {
        throw new System.NotImplementedException();
    }

    public bool GetJumpInput_Down()
    {
        return Input.GetButtonDown(InputConstants.k_ButtonNameJump);
    }

    public Vector2 GetLookInput()
    {
        Vector2 look = new Vector2(GetMouseLookAxis(InputConstants.k_MouseAxisNameHorizontal), -GetMouseLookAxis(InputConstants.k_MouseAxisNameVertical));
        return look;
    }

    public Vector2 GetMovementInput()
    {
            Vector2 move = new Vector2(Input.GetAxisRaw(InputConstants.k_AxisNameHorizontal), Input.GetAxisRaw(InputConstants.k_AxisNameVertical));
            // constrain move input to a maximum magnitude of 1, otherwise diagonal movement might exceed the max move speed defined
            move = Vector2.ClampMagnitude(move, 1);

            return move;
    }

    //--------------------------------------------------------------

    private float GetMouseLookAxis(string mouseInputName)
    {
        float i = Input.GetAxisRaw(mouseInputName);

        // apply sensitivity multiplier
        i *= lookSensitivity;

        // reduce mouse input amount to be equivalent to stick movement
        i *= 0.01f;

        return i;
    }
}

internal class MouseAndKeyboardBuilderInputHandler : IBuilderInputHandler
{
    public float panSensitivity;

    internal MouseAndKeyboardBuilderInputHandler()
    {
        panSensitivity = 1.0f;
    }

    public bool GetAltSelectInput_Down()
    {
        return Input.GetButtonDown(InputConstants.k_ButtonNameAim);
    }

    public float GetCameraZoomInput()
    {
        return Input.GetAxis(InputConstants.k_ButtonNameZoomOnMap) * 10f;
    }

    public bool GetExitBuilderModeInput_Down()
    {
        throw new System.NotImplementedException();
    }

    public bool GetExitBuilderModeInput_Held()
    {
        throw new System.NotImplementedException();
    }

    public Vector2 GetCameraPanInput()
    {
        Vector2 move = new Vector2(Input.GetAxisRaw(InputConstants.k_AxisNameHorizontal), Input.GetAxisRaw(InputConstants.k_AxisNameVertical));
        // constrain move input to a maximum magnitude of 1, otherwise diagonal movement might exceed the max move speed defined
        move = Vector2.ClampMagnitude(move, 1);

        return move;
    }

    public bool GetSelectInput_Down()
    {
        return Input.GetButtonDown(InputConstants.k_ButtonNameSelect);
    }


    public bool GetRotateStructureInput_Down()
    {
        return Input.GetButtonDown(InputConstants.k_ButtonNameRotateStructure);
    }

    public Vector2 GetCursorPosition()
    {
        return Input.mousePosition;
    }

    //--------------------------------------------------------------
    /*
    private float GetMouseLookAxis(string mouseInputName)
    {
        float i = Input.GetAxisRaw(mouseInputName);

        // apply sensitivity multiplier
        i *= lookSensitivity;

        // reduce mouse input amount to be equivalent to stick movement
        i *= 0.01f;

        return i;
    }*/
}