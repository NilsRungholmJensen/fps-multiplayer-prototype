﻿using Mirror;
using UnityEngine;
using System.Threading.Tasks;
using System;
using Cysharp.Threading.Tasks;

/// <summary>
/// Indicates that this GameObject has one or more Damagable components on its children,
/// and can be killed by bullets and projectiles.
/// </summary>
internal class Killable : NetworkBehaviour
{
    [SyncVar] [SerializeField] private float hitPoints = 100;
    [SyncVar] private bool dead = false;
    [SerializeField] private LootSource lootSource;

    public Action OnDeath;

    private void OnEnable()
    {
        //Subscribe to damage
        foreach (Damagable damagable in GetComponentsInChildren<Damagable>())
        { 
            damagable.OnDamaged += this.OnHit;
        }
    }

    private void OnDisable()
    {
        //Unsubscribe from damage
        foreach (Damagable damagable in GetComponentsInChildren<Damagable>())
        {
            damagable.OnDamaged -= this.OnHit;
        }
    }

    private void OnHit(float damage)
    {
        //Inform the server
        CmdOnHit(damage);
    }

    //Server only

    [Command(requiresAuthority = false)]
    private void CmdOnHit(float damage, NetworkConnectionToClient sender = null)
    {
        hitPoints -= damage;
        
        if (hitPoints <= 0)
        {
            dead = true;
            OnDeath?.Invoke();

            //Unsubscribe from damage
            foreach (Damagable damagable in GetComponentsInChildren<Damagable>())
            {
                damagable.OnDamaged -= this.OnHit;
            }

            //Inform all clients
            RpcOnDeath();

            if (lootSource != null)
                DropLoot();

            DestorySelfAfterDelay(5f);
        }
    }

    [Server]
    private void DropLoot()
    {
        RaycastHit hit;
        Physics.Raycast(transform.position, Vector3.down, out hit);
        Vector3 lootSpawnPoint = hit.point;

        (ItemDefinition, int)[] totalLoot = lootSource.Get();
        foreach((ItemDefinition, int) lootType in totalLoot)
        {
            for(int i=0; i<lootType.Item2; i++)
            {
                GameObject newLootPrefab = lootType.Item1.itemPrefab;
                GameObject newLoot = Instantiate(newLootPrefab, lootSpawnPoint + (UnityEngine.Random.insideUnitSphere * 0.2f), Quaternion.identity);
                NetworkServer.Spawn(newLoot);
            }
        }
    }

    [Server]
    private async UniTaskVoid DestorySelfAfterDelay(float delay)
    {
        await UniTask.Delay((int)delay * 1000);
        if (gameObject)
            NetworkServer.Destroy(gameObject);
    }

    //Clients only

    [ClientRpc]
    private void RpcOnDeath()
    {
        GetComponent<Animator>()?.Play("Die");
    }
}