﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "NewLootSource", menuName = "FPS Assets/LootSource")]
internal class LootSource : ScriptableObject
{
    [SerializeField] private List<LootType> droppedTypes;
    [HideInInspector] [SerializeField] private Dictionary<LootType, RandomDeck<int>> randomDropAmountDecks;

    public void OnValidate()
    {
        foreach (LootType lootType in droppedTypes)
        {
            List<(int, int)> entries = new List<(int, int)>();

            if (lootType.useNormalDistribution)
            {
                for (int i = lootType.minAmount; i <= lootType.maxAmount/4; i++)
                {
                    for (int j = lootType.minAmount; j <= lootType.maxAmount / 4; j++)
                    {
                        for (int k = lootType.minAmount; k <= lootType.maxAmount / 4; k++)
                        {
                            for (int l = lootType.minAmount; l <= lootType.maxAmount / 4; l++)
                            {
                                entries.Add((Mathf.RoundToInt(i+j+k+l), 1));
                            }
                        }
                    }
                }
            }

            else
            {
                for (int i = lootType.minAmount; i <= lootType.maxAmount; i++)
                {
                    entries.Add((i, 1));
                }
            }

            int repeatSetsPerDeck = Mathf.Max(lootType.repeatSetsPerDeck, 1);

            randomDropAmountDecks = new Dictionary<LootType, RandomDeck<int>>();
            randomDropAmountDecks.Add(lootType, new RandomDeck<int>(entries.ToArray(), repeatSetsPerDeck, Mathf.FloorToInt(repeatSetsPerDeck * 1.5f)));
        }
    }

    public (ItemDefinition, int)[] Get()
    {
        List<(ItemDefinition, int)> results = new List<(ItemDefinition, int)>();

        foreach (LootType lootType in droppedTypes)
        {
            RandomDeck<int> randomDeck;
            randomDropAmountDecks.TryGetValue(lootType, out randomDeck);
            results.Add((lootType.droppedItemDefinition, randomDeck.Get()));
        }

        return results.ToArray();
    }
}

[Serializable]
internal class LootType
{
    [SerializeField] internal ItemDefinition droppedItemDefinition;
    [Tooltip("The min amount of objects allowed to drop at a time.")]
    [SerializeField] [Range(0, 25)] internal int minAmount;
    [Tooltip("The max amount of objects allowed to drop at a time.")]
    [SerializeField] [Range(1, 25)] internal int maxAmount;
    [Tooltip("How many times each value between MinAmount and MaxAmount is allowed to repeat. If this is e.g. 5, you can technically drop the exact same count of this LootType, 5 times in a row. Higher numbers means more of a random/chaotic feel. Lower numbers means a more predictable randomness.")]
    [SerializeField] internal int repeatSetsPerDeck;
    [Tooltip("When checked, uses normal distribution. So instead of each possible value between MinAmount and MaxAmount being evenly likely, average numbers become more likely.")]
    [SerializeField] internal bool useNormalDistribution;
}

public class RandomDeck<T>
{
    private Stack<T> Stack => (stack != null) ? stack : (stack = new Stack<T>());
    private Stack<T> stack;

    private (T, int)[] deckEntries;
    private int minSetsPerDeck;
    private int maxSetsPerDeck;

    public RandomDeck((T, int)[] deckEntries, int minSetsPerDeck, int maxSetsPerDeck)
    {
        this.deckEntries = deckEntries;
        this.minSetsPerDeck = minSetsPerDeck;
        this.maxSetsPerDeck = maxSetsPerDeck;

        FillDeck(deckEntries, minSetsPerDeck, maxSetsPerDeck);
    }

    private void FillDeck((T, int)[] deckEntries, int minSetsPerDeck, int maxSetsPerDeck)
    {
        int setsPerDeck = UnityEngine.Random.Range(minSetsPerDeck, maxSetsPerDeck);

        foreach ((T, int) entry in deckEntries)
        {
            for (int i = 0; i < (entry.Item2 * setsPerDeck); i++)
                Stack.Push(entry.Item1);
        }

        Stack.OrderBy(x => UnityEngine.Random.value);
    }

    public T Get()
    {
        T result = Stack.Pop();

        if (Stack.Count <= 0)
        {
            FillDeck(deckEntries, minSetsPerDeck, maxSetsPerDeck);
        }

        return result;
    }
}