﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon Definition", menuName = "FPS Assets/Weapon Definition")]
internal class WeaponBlueprint : ScriptableObject
{
    [SerializeField] internal string displayName;
    [SerializeField] internal Sprite crosshairSprite;
    [SerializeField] internal float crosshairSize;
    [SerializeField] internal GameObject modelPrefab;
    [Tooltip("Translation to apply to weapon arm when aiming with this weapon")]
    [SerializeField] internal Vector3 aimOffset;

    [SerializeField] internal FiringActionType firingActionType;
    [SerializeField] internal ProjectileType projectileType;

    [Tooltip("The damage multiplier applied to each individual bullet. 1 = the damage provided by the bullet, 0.5 = half, 2.5 = 250% of that, etc.")]
    [SerializeField] internal float damageMultiplier = 1f;
    [Tooltip("The number of bullets fired in each round.")]
    [SerializeField] internal int bulletsPerRound = 1;
    [Tooltip("The time (in secs.) it takes for the weapon to reload between individual bullets. Determines the minimum time between shots.")]
    [SerializeField] internal float selfLoadTime = 0.15f;
    [Tooltip("For burst firing action: The time (in secs.) it takes for the weapon to prepare another burst. Determines the minumum time between bursts.")]
    [SerializeField] internal float burstLoadTime = 0.01f;
    [Tooltip("For burst firing action: The number of rounds in each burst.")]
    [SerializeField] internal int roundsPerBurst = 3;
    [SerializeField] internal GameObject debugHitEffect;
    [SerializeField] internal float aimZoomRatio = 0.8f;
    [SerializeField] internal float modelRecoil = 0.04f;
    [SerializeField] internal float cameraRecoil = 0.04f;

    void OnValidate()
    {
        roundsPerBurst = Mathf.Max(roundsPerBurst, 1);
        bulletsPerRound = Mathf.Max(bulletsPerRound, 1);
    }

    public enum FiringActionType
    {
        Semi = 0,
        Burst = 1,
        Auto = 2,
        Charged = 3,
    }

    public enum ProjectileType
    {
        Instant = 0,
        Launched = 1,
    }
}

/// <summary>
/// A weapon that instantly hit targets using a raycast, instead of spawning a projectile. Simulates infinitely fast bullets.
/// </summary>
/// 

