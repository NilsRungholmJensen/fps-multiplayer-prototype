﻿using Mirror;
using System;
using UnityEngine;

public abstract class AbstractPlayerFeature : NetworkBehaviour
{
    private bool initialised = false;

    public void Awake()
    {
        //If StartAuthority has somehow been called before Awake, don't disable
        if (!initialised)
            this.enabled = false;
    }

    protected void StartAuthority(AbstractPlayerController playerController)
    {
        //A Feature must only exist locally!
        this.enabled = hasAuthority;

        initialised = true;
    }

    protected void StopAuthority(AbstractPlayerController playerController)
    {
        this.enabled = false;
    }
}