﻿using Mirror;
using UnityEngine;

internal class ItemInstance : NetworkBehaviour
{
    [SerializeField] internal ItemDefinition itemDefinition;
}