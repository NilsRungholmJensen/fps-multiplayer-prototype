﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon Definition", menuName = "Builder Assets/Structure Definition")]
internal class StructureDefinition : ScriptableObject
{
    [SerializeField] internal string uniqueName;
    [SerializeField] internal GameObject structurePrefab;
    [SerializeField] internal int price;
}