﻿using UnityEngine;

public class CameraDragPanFeature : BuilderPlayerFeature, IBuilderPlayerDependant
{
    private void Update()
    {
        if (Input.GetButton(InputConstants.k_ButtonNameDragPan))
        {
            float sensitivity = 1f;
            float mouseX = Input.GetAxis(InputConstants.k_MouseAxisNameHorizontal);
            float mouseY = Input.GetAxis(InputConstants.k_MouseAxisNameVertical);
            PlayerController.playerCamera.transform.position += PlayerController.transform.right * (mouseX * -1) * sensitivity + PlayerController.transform.forward * (mouseY * -1) * sensitivity;
        }
    }
}