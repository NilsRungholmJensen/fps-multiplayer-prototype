﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Structure Library", menuName = "Builder Assets/Structure Library")]
internal class StructureLibrary : ScriptableObject
{
    [SerializeField] internal List<StructureDefinition> AvalibleStructures;
}