﻿using Mirror;
using System;
using UnityEngine;

[RequireComponent(typeof(Interactable), typeof(ItemInstance))]
public class ManualPickupable : NetworkBehaviour
{
    private Interactable Interactable => (interactable != null) ? interactable : (interactable = GetComponent<Interactable>());
    private Interactable interactable;

    private ItemInstance ItemInstance => (itemInstance != null) ? itemInstance : (itemInstance = GetComponent<ItemInstance>());
    private ItemInstance itemInstance;

    private void Awake()
    {
        Interactable.onInteraction += OnTryPickUp;
    }

    private void OnTryPickUp(FpsPlayerController obj)
    {
        CmdOnTryPickUp(obj.netIdentity);
    }

    [Command]
    private void CmdOnTryPickUp(NetworkIdentity netIdentity)
    {
        Debug.Log("Try pick up!");
    }
}