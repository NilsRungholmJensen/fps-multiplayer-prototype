﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPanController : MonoBehaviour, IBuilderPlayerDependant
{
    protected BuilderPlayerController playerController;

    private CameraPanZone[] CameraPanZones => (cameraPanZones != null) ? cameraPanZones : (cameraPanZones = GetComponentsInChildren<CameraPanZone>() ?? new CameraPanZone[0]);

    public Action<BuilderPlayerController> OnStartPlayerAuthority => StartAuthority;
    public Action<BuilderPlayerController> OnStopPlayerAuthority => StopAuthority;

    private CameraPanZone[] cameraPanZones;

    private void StartAuthority(AbstractPlayerController playerController)
    {
        this.playerController = (playerController as BuilderPlayerController);

        foreach (CameraPanZone camZone in CameraPanZones)
        {
            camZone.onMouseOver += OnCameraPanZoneHovered;
        }
    }

    private void StopAuthority(AbstractPlayerController playerController)
    {
        foreach (CameraPanZone camZone in CameraPanZones)
        {
            camZone.onMouseOver -= OnCameraPanZoneHovered;
        }
    }

    private void OnCameraPanZoneHovered(Vector2 panDirection)
    {
        playerController.playerCamera.transform.position += new Vector3(panDirection.x, 0f, panDirection.y) * 0.2f;
    }
}
