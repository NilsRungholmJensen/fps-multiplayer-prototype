﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ScreenLogger))]
public class NetworkManagerDebugSettings : MonoBehaviour
{
    public InitMode startEditorAs;
    public bool onScreenDebugInEditor;
    public bool lockCursorInEditor;

    public InitMode startBuildAs;
    public bool onScreenDebugInBuild;
    public bool lockCursorInBuild;

    public enum InitMode
    {
        None,
        Host,
        Client,
        Server
    }

    private NetworkManager Manager => manager ? manager : (manager = GetComponent<NetworkManager>());
    private NetworkManager manager;

    private bool CursorLocked
    {
        get { return cursorLocked; }
        set
        {
            cursorLocked = value;
            Cursor.lockState = cursorLocked ? CursorLockMode.Locked : CursorLockMode.None;
            Cursor.visible = !cursorLocked;
        }
    }

    private bool cursorLocked;

    // Start is called before the first frame update
    void Start()
    {
        InitMode initMode = Application.isEditor ? startEditorAs : startBuildAs;

        switch (initMode)
        {
            case InitMode.None: default:
                { } break;
            case InitMode.Host:
                { Manager?.StartHost(); } break;
            case InitMode.Client:
                { Manager?.StartClient(); }
                break;
            case InitMode.Server:
                { Manager?.StartServer(); }
                break;
        }

        //
        bool onScreenDebug = Application.isEditor ? onScreenDebugInEditor : onScreenDebugInBuild;

        GetComponent<ScreenLogger>()?.gameObject?.SetActive(onScreenDebug);

        CursorLocked = Application.isEditor ? lockCursorInEditor : lockCursorInBuild;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            CursorLocked = !CursorLocked;
        }
        if (Input.GetKeyDown(KeyCode.Delete))
        {
            GetComponent<ScreenLogger>().ClearLog();
        }
    }
}
