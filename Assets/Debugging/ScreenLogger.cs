﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenLogger : MonoBehaviour
{
    private GUIStyle style;
    private GUILayoutOption[] options;

    private const int MAXCHARS = 1000;
    private Queue logQueue = new Queue();
    void Start()
    {
        style = new GUIStyle
        {
            richText = true,
            alignment = TextAnchor.LowerLeft
        };

        options = new GUILayoutOption[]
        {
            GUILayout.ExpandHeight(true),
        };

        Debug.Log("--- ON-SCREEN LOG STARTED ---");
    }

    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        string[] logArguments = logString.Split('@');
        if (logArguments.Length > 1)
        {
            logString = logArguments[logArguments.Length-1];

            for (int i=0; i<logArguments.Length-1; i++)
            {
                string arg = logArguments[i];

                if (arg == "b")
                {
                    logString = "<b>" + logString + "</b>";
                }
                else if (arg == "i")
                {
                    logString = "<i>" + logString + "</i>";
                }
                else
                {
                    logString = "<color="+arg+">" + logString + "</color>";
                }
            }
        }

        if (type == LogType.Log)
            logQueue.Enqueue("\n > " + logString);
        else
            logQueue.Enqueue("\n [" + type + "] : " + logString);

        if (type == LogType.Exception)
            logQueue.Enqueue("\n" + stackTrace);
    }

    void Update()
    {
        while (logQueue.Count > 10)
            logQueue.Dequeue();
    }

    void OnGUI()
    {
        GUILayout.BeginVertical(GUILayout.Height(Screen.height));
        GUILayout.FlexibleSpace();

        string concatenatedString = "";
        Queue tempLogQueue = new Queue(logQueue);

        for (int i=0; i<logQueue.Count; i++)
        {
            concatenatedString += tempLogQueue.Dequeue();
        }

        GUILayout.Label(concatenatedString, style, new GUILayoutOption[] { });
        GUILayout.EndVertical();
    }

    internal void ClearLog()
    {
        logQueue.Clear();
    }
}