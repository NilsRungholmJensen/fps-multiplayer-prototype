﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DebugShootingFeature : NetworkBehaviour
{
    private IFpsInputHandler InputHandler => (inputHandler != null) ? inputHandler : (inputHandler = GetComponent<FpsInputProvider>().Handler);
    private IFpsInputHandler inputHandler;

    private FpsPlayerController PlayerController => (playerController != null) ? playerController : (playerController = GetComponent<FpsPlayerController>());
    private FpsPlayerController playerController;

    private void Start()
    {
        //Feature must only exist locally!
        this.enabled = isLocalPlayer;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 aimOrigin = PlayerController.AimOrigin;
        Vector3 aimDirection = PlayerController.AimDirection;

        if (InputHandler.GetFireInput_Down())
        {
            TryDebugShoot(aimOrigin, aimDirection);
        }
    }

    private void TryDebugShoot(Vector3 aimOrigin, Vector3 aimDirection)
    {
        RaycastHit hit;

        if (Physics.Raycast(aimOrigin, aimDirection, out hit, 100f, LayerMask.GetMask("Default")))
        {
            Hittable other = hit.collider.GetComponent<Hittable>();
            if (other)
            {
                Debug.Log("Olive@" + (GetComponent<NetworkIdentity>().isLocalPlayer ? "Local player TRIED debug-shooting " : "Remote player TRIED debug-shooting ") + other.gameObject.name);
                CmdTryDebugShoot(aimOrigin, aimDirection);
            }
        }
    }

    [Command]
    private void CmdTryDebugShoot(Vector3 aimOrigin, Vector3 aimDirection, NetworkConnectionToClient sender = null)
    {
        RaycastHit hit;

        if (Physics.Raycast(aimOrigin, aimDirection, out hit, 100f, LayerMask.GetMask("Default")))
        {
            Hittable other = hit.collider.GetComponent<Hittable>();
            if (other)
            {
                Debug.Log("Navy@" + (GetComponent<NetworkIdentity>().isLocalPlayer ? "Local player (ConnId "+sender.connectionId+") DID debug-shoot " : "Remote player (ConnId " + sender.connectionId + ") DID debug-shoot ") + other.gameObject.name);
                RpcConfirmDebugShoot(sender.connectionId);
            }
        }
    }

    [ClientRpc]
    private void RpcConfirmDebugShoot(int requesterId)
    {
        Debug.Log("Green@" + (GetComponent<NetworkIdentity>().isLocalPlayer ? "Local player (ConnId " + requesterId + ") got debug-shoot confirmed" : "Remote player (ConnId " + requesterId + ") got debug-shoot confirmed"));
    }
}
