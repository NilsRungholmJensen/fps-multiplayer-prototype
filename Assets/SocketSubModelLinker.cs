﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Links a Socket-component and any GameObject (e.g. a piece of a structure's model) so that the GO is only visible when the socket is connected. The correlation can be inverted by checking "invertActivation".
/// </summary>
internal class SocketSubModelLinker : MonoBehaviour
{
    [SerializeField] List<SocketGameObjectPair> socketModelPairs = new List<SocketGameObjectPair>();
    private Dictionary<SocketGameObjectPair, Action> onConnectDelegateDictionary;
    private Dictionary<SocketGameObjectPair, Action> onDisconnectDelegateDictionary;

    private void OnEnable()
    {
        onConnectDelegateDictionary = socketModelPairs.ToDictionary((x) => x, (x) => new Action(() => x.subModel.SetActive(!x.invertActivation)));
        onDisconnectDelegateDictionary = socketModelPairs.ToDictionary((x) => x, (x) => new Action(() => x.subModel.SetActive(x.invertActivation)));

        foreach (SocketGameObjectPair pair in socketModelPairs)
        {
            pair.socket.OnConnected += onConnectDelegateDictionary[pair];
            pair.socket.OnDisconnected += onDisconnectDelegateDictionary[pair];

            pair.subModel.SetActive(pair.invertActivation);
        }
    }

    private void OnDisable()
    {
        foreach (SocketGameObjectPair pair in socketModelPairs)
        {
            pair.socket.OnConnected -= onConnectDelegateDictionary[pair];
            pair.socket.OnDisconnected -= onDisconnectDelegateDictionary[pair];

            pair.subModel.SetActive(pair.invertActivation);
        }
    }
}

[Serializable]
internal struct SocketGameObjectPair
{
    [SerializeField] internal Socket socket;
    [SerializeField] internal GameObject subModel;
    [SerializeField] internal bool invertActivation;
}