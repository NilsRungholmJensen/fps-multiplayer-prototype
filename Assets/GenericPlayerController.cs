﻿using System.Collections;
using System.Linq;
using UnityEngine;
using Mirror;
using System;

public class GenericPlayerController : AbstractPlayerController
{
    internal Action onShowHidePauseMenu;

    private void OnEnable()
    {
        foreach (IGenericPlayerDependant playerDependant in UnityEngineExtensions.FindAllComponentsInScene().OfType<IGenericPlayerDependant>())
        {   
            playerDependant.OnStartPlayerAuthority?.Invoke(this);
        }
    }

    private void OnDisable()
    {
        foreach (IGenericPlayerDependant playerDependant in UnityEngineExtensions.FindAllComponentsInScene().OfType<IGenericPlayerDependant>())
        {
            playerDependant.OnStopPlayerAuthority?.Invoke(this);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            onShowHidePauseMenu?.Invoke();
        }
    }
}