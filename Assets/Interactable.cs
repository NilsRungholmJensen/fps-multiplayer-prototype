﻿using System;
using UnityEngine;
using Mirror;

internal class Interactable : LayerMonoBehavior, ILayerSetter
{
    [SerializeField] internal string interactionVerb;
    [SerializeField] internal float executionTime;

    public override int Layer => LayerMask.NameToLayer("Interactable");
    
    internal Action<FpsPlayerController> onInteraction;

    internal void OnInteraction(FpsPlayerController callingPlayerObject)
    {
        onInteraction?.Invoke(callingPlayerObject);
    }
}