﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomFeature : BuilderPlayerFeature, IBuilderPlayerDependant
{
    private void Update()
    {
        float zoomDelta = InputHandler.GetCameraZoomInput();

        Vector3 cameraPosition = PlayerController.playerCamera.transform.localPosition;
        PlayerController.playerCamera.transform.localPosition = cameraPosition + new Vector3(0f, 0f, zoomDelta * 2.5f);
    }
}