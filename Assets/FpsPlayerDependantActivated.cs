﻿using System;
using UnityEngine;

/// <summary>
/// Placed on objects to automatically activate/deactivate them along with the FpsPlayer object (based on whether it has authority)
/// </summary>
public class FpsPlayerDependantActivated : MonoBehaviour, IFpsPlayerDependant
{
    [SerializeField] private bool startOn;

    public Action<FpsPlayerController> OnStartPlayerAuthority => StartAuthority;
    public Action<FpsPlayerController> OnStopPlayerAuthority => StopAuthority;
    
    private void Awake()
    {
        gameObject.SetActive(startOn);
    }

    private void StartAuthority(FpsPlayerController playerController)
    {
        gameObject.SetActive(true);
    }

    private void StopAuthority(FpsPlayerController playerController)
    {
        gameObject.SetActive(false);
    }
}