﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GlobalServerController : NetworkBehaviour
{
    //Serialized fields

    [SerializeField] internal GameObject builderPlayerPrefab;

    //Static fields

    internal static int TotalCurrency {
        get { return Instance.totalCurrency; }
        set { Instance.totalCurrency = value; }
    }

    private static Dictionary<NetworkConnection, GameObject> registeredFpsPlayerObjects = new Dictionary<NetworkConnection, GameObject>();
    private static Dictionary<NetworkConnection, GameObject> registeredBuilderPlayerObjects = new Dictionary<NetworkConnection, GameObject>();

    //Instance fields

    private static GlobalServerController Instance => (instance != null) ? instance : (instance = FindObjectOfType<GlobalServerController>());
    private static GlobalServerController instance;

    private NavMeshSurface NavMeshSurface => (navMeshSurface != null) ? navMeshSurface : (navMeshSurface = FindObjectOfType<NavMeshSurface>());
    private NavMeshSurface navMeshSurface;

    private int totalCurrency;

    internal static void TransitionToBuilderPlayer(NetworkIdentity playerIdentity)
    {
        NetworkConnection connection = playerIdentity.connectionToClient;

        //Try adding existing FpsPlayer obj
        if (!registeredFpsPlayerObjects.ContainsKey(connection))
            registeredFpsPlayerObjects.Add(connection, connection.identity.gameObject);

        //Try getting existing BuilderPlayer obj
        GameObject existingBuilderPlayerObject;
        registeredBuilderPlayerObjects.TryGetValue(connection, out existingBuilderPlayerObject);

        //If existing builder player object not found: Create it
        if (!existingBuilderPlayerObject)
        {
            GameObject newBuilderPlayerObject = Instantiate(Instance.builderPlayerPrefab);
            NetworkServer.Spawn(newBuilderPlayerObject);

            newBuilderPlayerObject.GetComponent<NetworkIdentity>().AssignClientAuthority(connection);

            registeredBuilderPlayerObjects.Add(connection, newBuilderPlayerObject);
            existingBuilderPlayerObject = newBuilderPlayerObject;
        }

        NetworkServer.ReplacePlayerForConnection(connection, existingBuilderPlayerObject, false);
    }

    internal static void TransitionToFpsPlayer(NetworkIdentity playerIdentity)
    {
        NetworkConnection connection = playerIdentity.connectionToClient;

        //Try adding existing BuilderPlayer obj
        if (!registeredBuilderPlayerObjects.ContainsKey(connection))
            registeredBuilderPlayerObjects.Add(connection, connection.identity.gameObject);

        //Try getting existing FpsPlayer obj
        GameObject existingFpsPlayerObject;
        registeredFpsPlayerObjects.TryGetValue(connection, out existingFpsPlayerObject);

        //If existing fps player object not found: Create it
        if (!existingFpsPlayerObject)
        {
            throw new NotImplementedException("Spawning an FpsPlayer is not implemented yet. Make sure to start with an FpsPlayer and transition to BuilderPlayer.");
        }

        NetworkServer.ReplacePlayerForConnection(connection, existingFpsPlayerObject, false);
    }

    //TODO: Allow other scripts to mark navmesh as dirty, and add a minimum time before immediate rebakes are allowed (so we don't do them 10 frames in a row).
    internal static void RebakeLevelNavmesh()
    {
        Instance.NavMeshSurface.BuildNavMesh();
    }
}
