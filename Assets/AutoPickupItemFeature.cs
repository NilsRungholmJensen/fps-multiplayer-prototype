﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Feature component, allowing a player to auto-pick up objects containing an AutoPickupable
/// </summary>
public class AutoPickupItemFeature : FpsPlayerFeature
{
    private void OnTriggerEnter(Collider trigger)
    {
        AutoPickupable autoPickupable = trigger.gameObject.GetComponentInParent<AutoPickupable>();
        if (autoPickupable)
        {
            autoPickupable.OnTryPickUp(PlayerController);
        }
    }
}
