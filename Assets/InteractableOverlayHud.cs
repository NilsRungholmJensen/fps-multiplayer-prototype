﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;

public class InteractableOverlayHud : MonoBehaviour, IFpsPlayerDependant
{
    public Action<FpsPlayerController> OnStartPlayerAuthority => StartAuthority;
    public Action<FpsPlayerController> OnStopPlayerAuthority => StopAuthority;

    [SerializeField] internal Image textFrame;
    [SerializeField] internal TextMeshProUGUI textComponent;
    
    private InteractWithItemFeature playerFeature;

    private void StartAuthority(FpsPlayerController playerController)
    { 
        playerFeature = playerController.gameObject.GetComponent<InteractWithItemFeature>();

        if (!playerFeature)
            return;

        playerFeature.onInteractableHovered += OnInteractableHovered;
        playerFeature.onNoInteractableHovered += OnNoInteractableHovered;
    }

    private void StopAuthority(FpsPlayerController playerController)
    {
        playerFeature = playerController.gameObject.GetComponent<InteractWithItemFeature>();

        if (!playerFeature)
            return;

        playerFeature.onInteractableHovered -= OnInteractableHovered;
        playerFeature.onNoInteractableHovered -= OnNoInteractableHovered;
    }

    private void OnNoInteractableHovered()
    {
        textFrame.enabled = false;
        textComponent.enabled = false;
    }

    private void OnInteractableHovered(Interactable interactable)
    {
        if (!interactable)
            return;

        textFrame.enabled = true;
        textComponent.enabled = true;

        textComponent.text = "[F] "+interactable.interactionVerb;
    }
}
