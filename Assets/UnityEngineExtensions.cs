﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class UnityEngineExtensions
{
    public static List<Component> FindAllComponentsInScene()
    {
        List<Component> componentsInScene = new List<Component>();
        
        foreach (GameObject go in FindAllGameObjectsInScene())
        {
            Component[] components = go.GetComponents(typeof(Component));
            foreach (Component com in components)
            {
                componentsInScene.Add(com);
            }
        }

        return componentsInScene;
    }

    public static List<GameObject> FindAllGameObjectsInScene()
    {
        List<GameObject> objectsInScene = new List<GameObject>();

        foreach (GameObject go in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[])
        {
            if (go.transform.root.gameObject.scene.IsValid() && !(go.hideFlags == HideFlags.NotEditable || go.hideFlags == HideFlags.HideAndDontSave))
                objectsInScene.Add(go);
        }

        return objectsInScene;
    }
}