﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraPanZone : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Vector2 cameraDirection;
    internal Action<Vector2> onMouseOver;
    private bool mouseOver;

    private void OnDisable()
    {
        mouseOver = false;
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        mouseOver = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        mouseOver = true;
    }

    private void Update()
    {
        if (mouseOver)
            onMouseOver?.Invoke(cameraDirection);
    }
}
