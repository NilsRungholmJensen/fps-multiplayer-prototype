﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Animations;

public class AutoAnimateRotation : MonoBehaviour
{
    [SerializeField] private Axis axisToRotateAround;
    [SerializeField] private float speed;

    [HideInInspector] [SerializeField]  private Vector3 rotationEuler;

    private void Awake()
    {
        StartToRotate();
    }

    private async UniTaskVoid StartToRotate()
    {
        rotationEuler = new Vector3(
            axisToRotateAround == Axis.X ? speed * 1000f : 0f,
            axisToRotateAround == Axis.Y ? speed * 1000f : 0f,
            axisToRotateAround == Axis.Z ? speed * 1000f : 0f
            );

        while (true)
        {
            transform.Rotate(rotationEuler * Time.deltaTime);
            await UniTask.Yield();
        }
    }

    private void OnValidate()
    {
        rotationEuler = new Vector3(
             axisToRotateAround == Axis.X ? speed * 1000f : 0f,
             axisToRotateAround == Axis.Y ? speed * 1000f : 0f,
             axisToRotateAround == Axis.Z ? speed * 1000f : 0f
             );
    }
}
