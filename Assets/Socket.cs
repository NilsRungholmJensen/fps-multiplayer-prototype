﻿using System;
using System.Collections.Generic;
using UnityEngine;

internal class Socket : MonoBehaviour
{
    enum SocketType
    {
        Input = 0,
        Output = 1,
        Ambivalent = 2,
    }

    [SerializeField] SocketType Type;
    List<ItemDefinition> AllowedItemDefinitions => throw new NotImplementedException();
    Socket Connection;

    private float GridSize => StructureGrid.CellSize;
    internal (int, int) OuterCell => GetCellBehindTransform();
    //private (int, int)? outerCell;

    internal (int, int) InnerCell => GetCellInFrontOfTransform();
    //private (int, int)? innerCell;

    public GridRotation InwardsDirection => GetInwardsDirection();
    public GridRotation OutwardsDirection => GetOutwardsDirection();

    public Action OnConnected { get; internal set; }
    public Action OnDisconnected { get; internal set; }
    public bool IsConnected { get; internal set; }

    private GridRotation GetInwardsDirection()
    {
        return transform.rotation.ToGridRotation();
    }

    private GridRotation GetOutwardsDirection()
    {
        return (GridRotation)(((int)transform.rotation.ToGridRotation() + 2) % 4);
    }

    private (int, int) GetCellBehindTransform()
    {
        return (transform.position + (transform.forward * GridSize * -0.5f)).ToGridPosition();
    }

    private (int, int) GetCellInFrontOfTransform()
    {
        return (transform.position + (transform.forward * GridSize * 0.5f)).ToGridPosition();
    }

    private void OnDrawGizmosSelected()
    {
        Vector3 outerCellPos = (transform.position + transform.forward * GridSize * -0.5f).ToGridPositionAsVector();
        Vector3 innerCellPos = (transform.position + transform.forward * GridSize * 0.5f).ToGridPositionAsVector();
        //Origin cell
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(outerCellPos + (innerCellPos - outerCellPos) * GridSize * 0.15f, GridSize * 0.1f);
        //Target cell
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(innerCellPos - (innerCellPos - outerCellPos) * GridSize * 0.15f, GridSize * 0.1f);
    }

    internal bool TryConnect(Socket other)
    {
        //Crude debugging
        if (IsConnected || Type != SocketType.Ambivalent && other.Type != SocketType.Ambivalent && (Type != SocketType.Input || other.Type != SocketType.Output) && (Type != SocketType.Output || other.Type != SocketType.Input))
            return false;

        EstablishConnection(other);
        other.EstablishConnection(this);

        return true;
    }

    private void EstablishConnection(Socket socket)
    {
        IsConnected = true;
        OnConnected?.Invoke();
    }
}